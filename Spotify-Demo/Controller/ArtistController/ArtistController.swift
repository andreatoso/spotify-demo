//
//  ArtistController.swift
//  Spotify-Demo
//
//  Created by Andrea Toso on 29/08/18.
//  Copyright © 2018 Andrea Toso. All rights reserved.
//

import UIKit

class ArtistController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    var backgroundColorEffectConstraint: NSLayoutConstraint?
    var gradientViewConstraint: NSLayoutConstraint?
    var shuffleButtonConstraint: NSLayoutConstraint?
    var isScrollViewScrolling = false
    
    var config = ArtistConfig()
    var artist: Artist?
    var artistDetails: ArtistDetails?
    
    weak var delegate: PlayerDelegate?
    var trackController: TrackController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupController()
        setupNavigationBar()
        setupViews()
    }
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 6
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if artistDetails == nil {return 0}
        switch section {
        case 0:
            return self.artistDetails?.latestRelease == nil ? 0 : 1
        case 1:
            return self.artistDetails?.playlist == nil ? 0 : 1
        case 2:
            guard let tracks = artistDetails?.topTracks else {return 0}
            return tracks.count > 5 ? 5 : 0
        case 3:
            guard let artists = artistDetails?.similarArtists else {return 0}
            return artists.isEmpty ? 0 : 1
        case 5:
            guard let albums = artistDetails?.albums else {return 0}
            return albums.count > 2 ? 2 : albums.count
        default:
            return 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch indexPath.section {
        case 0:
            return CGSize(width: view.frame.size.width, height: 50)
        case 1:
            return CGSize(width: view.frame.size.width, height: 100)
        case 5:
            return CGSize(width: view.frame.size.width/2, height: 240)
        default:
            return CGSize(width: view.frame.size.width, height: 50)
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch indexPath.section {
        case 0:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: config.LATEST_RELEASE_ID, for: indexPath) as! ArtistLatestReleaseCell
            guard let artistDetails = self.artistDetails?.latestRelease else {return cell}
            cell.setupLatestRelease(album: artistDetails)
            return cell
        case 1:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: config.ARTIST_PLAYLIST_ID, for: indexPath) as! ArtistPlaylistCell
            guard let artistDetails = self.artistDetails?.playlist else {return cell}
            guard let artist = self.artist else {return cell}
            cell.setupArtistPlaylist(playlist: artistDetails, artist: artist)
            return cell
        case 2:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: config.POPULAR_SONGS_ID, for: indexPath) as! ArtistPopularSongCell
            guard let tracks = self.artistDetails?.topTracks else {return cell}
            cell.setupPopuplarTrack(track: tracks[indexPath.item])
            cell.songNumber.text = "\(indexPath.item+1)"
            return cell
        case 3:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: config.SIMILAR_ARTIST_ID, for: indexPath) as! ArtistSimilarCell
            guard let artistDetails = self.artistDetails?.similarArtists else {return cell}
            cell.setupSimilarArtists(artists: artistDetails)
            return cell
        case 4:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: config.ARTIST_TOUR_ID, for: indexPath) as! ArtistTourCell
            return cell
        default:
            let ids = [config.ARTIST_ALBUM_LEFT_ID, config.ARTIST_ALBUM_RIGHT_ID]
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ids[indexPath.item%2], for: indexPath) as! ArtistAlbumCell
            guard let artistDetails = self.artistDetails?.albums else {return cell}
            cell.setupCellPosition(indexPath: indexPath)
            cell.setupArtistAlbum(album: artistDetails[indexPath.item])
            return cell
        }
        
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let libraryController = LibraryController(collectionViewLayout: UICollectionViewFlowLayout())
        libraryController.delegate = self.delegate
        libraryController.trackController = self.trackController
        
        switch indexPath.section {
        case 0:
            guard let albums = self.artistDetails?.albums else {return}
            libraryController.library = Library(id: albums[indexPath.item].id, type: .album)
            self.navigationController?.pushViewController(libraryController, animated: true)
        case 1:
            guard let playlist = self.artistDetails?.playlist else {return}
            libraryController.library = Library(id: playlist.id, type: .playlist)
            self.navigationController?.pushViewController(libraryController, animated: true)
        case 2:
            guard let tracks = self.artistDetails?.topTracks else {return}
            delegate?.presentPlayer(for: tracks, at: IndexPath(item: indexPath.item, section: 0))
        case 5:
            guard let albums = self.artistDetails?.albums else {return}
            libraryController.library = Library(id: albums[indexPath.item].id, type: .album)
            self.navigationController?.pushViewController(libraryController, animated: true)
        default:
            break
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 15
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: 5, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        if section == 5 {
            return 0
        }
        return 10
    }
    
    let titleView: UILabel = {
        let title = UILabel()
        title.text = "Post Malone"
        title.textColor = UIColor.white
        title.font = UIFont.init(name: Font.Bold, size: 16)
        title.isHidden = true
        title.translatesAutoresizingMaskIntoConstraints = false
        return title
    }()
    
    let artistView: ArtistView = {
        let artistView = ArtistView()
        artistView.translatesAutoresizingMaskIntoConstraints = false
        return artistView
    }()
    
    let artistViewCopy: ArtistView = {
        let artistView = ArtistView()
        artistView.layer.zPosition = -1
        artistView.isHidden = true
        artistView.isUserInteractionEnabled = false
        artistView.translatesAutoresizingMaskIntoConstraints = false
        return artistView
    }()
    
    let backgroundColorEffect: UIView = {
        let view = UIView()
        view.backgroundColor = Color.Black
        view.layer.zPosition = -1
        view.isUserInteractionEnabled = false
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let gradientView: UIImageView = {
       let iv = UIImageView()
        iv.isUserInteractionEnabled = false
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    let shuffleButton: SpotifyButton = {
        let button = SpotifyButton()
        button.setTitle("RIPRODUZIONE CASUALE", for: .normal)
        button.layer.zPosition = 9999
        return button
    }()
    
    let shuffleGradient: UIImageView = {
        let iv = UIImageView()
        iv.isHidden = true
        iv.isUserInteractionEnabled = false
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fetchData {
            for section in 0...self.collectionView.numberOfSections-1 {
                self.findFirstSection(section: section)
            }
        }
    }
    
}

extension ArtistController {
    
    internal func fetchData(completion: @escaping () -> ()) {
        guard let artist = artist else {return}
        Service.sharedInstance.spotifyService.getArtistDetails(artist: artist) { (details) in
            self.artistDetails = details
            DispatchQueue.main.async {
                self.collectionView?.reloadData()
                completion()
            }
        }
        setAvarageColor()
        setupArtistInfo()
    }
    
    fileprivate func setupArtistInfo() {
        guard let artist = artist else {return}
        artistView.name.text = artist.name
        let followers = "\(artist.followers)".decimalFormat
        artistView.followers.text = "\(followers) FOLLOWERS"
        artistView.formatFollowersStyle(for: .artist)
        artistView.profileImage.loadImage(urlString: artist.profileImageUrl)
        setupArtistViewCopy()
    }
    
    fileprivate func setupArtistViewCopy() {
        if let text = self.artistView.name.text, let followers = self.artistView.followers.text {
            artistViewCopy.name.text = text
            artistViewCopy.followers.text = followers
            artistViewCopy.formatFollowersStyle(for: .artist)
        }
        artistViewCopy.profileImage.image = self.artistView.profileImage.image
    }
    
    fileprivate func setAvarageColor() {
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.5, animations: {
                self.backgroundColorEffect.backgroundColor = self.artistView.profileImage.image?.getAvarageColor()
            })
        }
    }
    
    internal func findFirstSection(section: Int) {
        if section == 0 {
            if collectionView.numberOfItems(inSection: section) > 0 && !config.FIRST_SECTION_FOUND {
                config.FIRST_SECTION = section
                config.FIRST_SECTION_FOUND = true
            }
        } else {
            if collectionView.numberOfItems(inSection: section-1) == 0 && collectionView.numberOfItems(inSection: section) > 0 && !config.FIRST_SECTION_FOUND {
                config.FIRST_SECTION = section
                config.FIRST_SECTION_FOUND = true
            }
        }
    }
    
}
