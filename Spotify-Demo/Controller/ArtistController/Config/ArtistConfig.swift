//
//  ArtistConfig.swift
//  Spotify-Demo
//
//  Created by Andrea Toso on 04/09/18.
//  Copyright © 2018 Andrea Toso. All rights reserved.
//

import Foundation

struct ArtistConfig {
    
    let FIRST_HEADER_ID = "FIRST_HEADER_ID"
    let HEADER_ID = "HEADER_ID"
    let FOOTER_ID = "FOOTER_ID"
    let LATEST_RELEASE_ID = "LATEST_RELEASE_ID"
    let ARTIST_PLAYLIST_ID = "ARTIST_PLAYLIST_ID"
    let POPULAR_SONGS_ID = "POPULAR_SONGS_ID"
    let SIMILAR_ARTIST_ID = "SIMILAR_ARTIST_ID"
    let ARTIST_TOUR_ID = "ARTIST_TOUR_ID"
    let ARTIST_ALBUM_LEFT_ID = "ARTIST_ALBUM_LEFT_ID"
    let ARTIST_ALBUM_RIGHT_ID = "ARTIST_ALBUM_RIGHT_ID"
    var FIRST_SECTION = 0
    var FIRST_SECTION_FOUND = false
    
}
