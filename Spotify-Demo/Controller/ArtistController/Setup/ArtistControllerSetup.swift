//
//  ArtistControllerSetup.swift
//  Spotify-Demo
//
//  Created by Andrea Toso on 30/08/18.
//  Copyright © 2018 Andrea Toso. All rights reserved.
//

import UIKit

// MARK: - Setup
extension ArtistController {
    
    fileprivate func setupCells() {
        collectionView?.register(ArtistLatestReleaseCell.self, forCellWithReuseIdentifier: config.LATEST_RELEASE_ID)
        collectionView?.register(ArtistPlaylistCell.self, forCellWithReuseIdentifier: config.ARTIST_PLAYLIST_ID)
        collectionView?.register(ArtistPopularSongCell.self, forCellWithReuseIdentifier: config.POPULAR_SONGS_ID)
        collectionView?.register(ArtistSimilarCell.self, forCellWithReuseIdentifier: config.SIMILAR_ARTIST_ID)
        collectionView?.register(ArtistTourCell.self, forCellWithReuseIdentifier: config.ARTIST_TOUR_ID)
        collectionView?.register(ArtistAlbumCell.self, forCellWithReuseIdentifier: config.ARTIST_ALBUM_LEFT_ID)
        collectionView?.register(ArtistAlbumCell.self, forCellWithReuseIdentifier: config.ARTIST_ALBUM_RIGHT_ID)
    }
    
    internal func setupController() {
        collectionView?.backgroundColor = Color.Black
        collectionView?.showsVerticalScrollIndicator = false
        collectionView?.contentInset = UIEdgeInsets.init(top: 270, left: 0, bottom: 15, right: 0)
        
        setupHeader()
        setupFooter()
        setupCells()
    }
    
}

// MARK: - Header
extension ArtistController {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        if collectionView.numberOfItems(inSection: section) == 0 {
            return .zero
        }
        if section == config.FIRST_SECTION {
            return CGSize(width: view.frame.size.width, height: 45 + 57)
        }
        return CGSize(width: view.frame.size.width, height: 45 + 20)
    }
    
    fileprivate func setupHeader() {
        collectionView?.register(SectionHeader.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: config.FIRST_HEADER_ID)
        collectionView?.register(SectionHeader.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: config.HEADER_ID)
    }
    
}

// MARK: - Footer
extension ArtistController {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        if collectionView.numberOfItems(inSection: section) == 0 {
            return .zero
        }
        if section == 4 || section == 5 {
            return CGSize(width: view.frame.size.width, height: 45 + 14)
        }
        return .zero
    }
    
    fileprivate func setupFooter() {
        collectionView?.register(ArtistCellFooter.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: config.FOOTER_ID)
    }
    
}

// MARK: - Setup Header and Footer
extension ArtistController {
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if kind == UICollectionView.elementKindSectionHeader {
            var ids: [String] = []
            for _ in 0...collectionView.numberOfSections-2 {ids.append(config.HEADER_ID)}
            ids.insert(config.FIRST_HEADER_ID, at: config.FIRST_SECTION)
            
            let headerTitles = ["Ultime uscite", "Scelto dall'artista", "Popolari", "Ai fan piace anche", "In tour", "Album"]
            let header = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: ids[indexPath.section], for: indexPath) as! SectionHeader
            header.title.text = headerTitles[indexPath.section]
            header.backgroundColor = Color.Black
            return header
        } else {
            let footer = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: config.FOOTER_ID, for: indexPath) as! ArtistCellFooter
            footer.backgroundColor = Color.Black
            if indexPath.section == 4 {
                footer.seeMore.text = "Vedi tutti i concerti"
            } else {
                footer.seeMore.text = "Vedi tutti gli album"
            }
            return footer
        }
    }
    
}
