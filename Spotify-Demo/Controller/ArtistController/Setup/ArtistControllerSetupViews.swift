//
//  ArtistControllerSetup.swift
//  Spotify-Demo
//
//  Created by Andrea Toso on 30/08/18.
//  Copyright © 2018 Andrea Toso. All rights reserved.
//

import UIKit

extension ArtistController {
    
    internal func setupNavigationBar() {
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true

        titleView.text = artist?.name
        navigationItem.titleView = titleView
        setupBackButtonItem()
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "dots"), style: .plain, target: self, action: nil)
    }
    
    internal func setupViews() {
        setupArtistView()
        setupShuffleButton()
        setupGradient()
    }
    
    fileprivate func setupArtistView() {
        guard let topPadding = collectionView?.contentInset.top else {return}
        guard let topAnchor = collectionView?.topAnchor else {return}
        
        collectionView?.addSubview(artistView)
        artistView.topAnchor.constraint(equalTo: topAnchor, constant: -topPadding + 30).isActive = true
        artistView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        artistView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        artistView.heightAnchor.constraint(equalToConstant: 270).isActive = true
        
        guard let navBarHeight = navigationController?.navigationBar.frame.size.height else {return}
        let statusBarHeight = UIApplication.shared.statusBarFrame.size.height
        
        collectionView?.insertSubview(artistViewCopy, at: 0)
        artistViewCopy.anchor(view.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 10 + navBarHeight + statusBarHeight, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 270 - 50)
        
        let underlayView = UIView(frame: view.frame)
        underlayView.backgroundColor = Color.Black
        underlayView.layer.zPosition = -1
        underlayView.isUserInteractionEnabled = false
        collectionView?.insertSubview(underlayView, at: 1)
    }
    
    fileprivate func setupShuffleButton() {
        guard let topAnchor = collectionView?.topAnchor else {return}
        guard let navBarHeight = navigationController?.navigationBar.frame.size.height else {return}
        let statusBarHeight = UIApplication.shared.statusBarFrame.size.height
        let totalHeight = navBarHeight + statusBarHeight
        
        collectionView?.addSubview(shuffleButton)
        shuffleButton.anchorCenterXToSuperview()
        shuffleButton.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -75).isActive = true
        shuffleButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        shuffleButtonConstraint = shuffleButton.topAnchor.constraint(equalTo: topAnchor, constant: -25)
        shuffleButtonConstraint?.isActive = true
        
        let gradient = CAGradientLayer()
        view.layoutIfNeeded()
        gradient.frame = view.bounds
        gradient.colors = [UIColor.black.withAlphaComponent(1).cgColor, UIColor.black.withAlphaComponent(0.4).cgColor, UIColor.clear.cgColor, ]
        gradient.locations = [0.0, 0.5, 1.0]
        
        shuffleGradient.image = gradient.image()
        collectionView?.insertSubview(shuffleGradient, belowSubview: shuffleButton)
        shuffleGradient.layer.zPosition = shuffleButton.layer.zPosition - 1
        shuffleGradient.anchor(view.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: totalHeight + 25, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 25)
    }
    
    fileprivate func setupGradient() {
        guard let topAnchor = collectionView?.topAnchor else {return}
        
        collectionView?.insertSubview(backgroundColorEffect, at: 0)
        backgroundColorEffect.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        backgroundColorEffect.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        backgroundColorEffect.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        backgroundColorEffectConstraint = backgroundColorEffect.bottomAnchor.constraint(equalTo: topAnchor)
        backgroundColorEffectConstraint?.isActive = true
        
        let gradient = CAGradientLayer()
        view.layoutIfNeeded()
        gradient.frame = backgroundColorEffect.bounds
        gradient.colors = [UIColor.clear.cgColor, Color.Black.cgColor]
        gradient.locations = [0.0, 1.0]
        
        backgroundColorEffect.insertSubview(gradientView, at: 1)
        gradientView.image = gradient.image()
        gradientView.anchor(view.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        gradientViewConstraint = gradientView.bottomAnchor.constraint(equalTo: topAnchor)
        gradientViewConstraint?.isActive = true
    }
    
    fileprivate func setupBackButtonItem() {
        let origImage = UIImage(named: "left_arrow")?.withInsets(UIEdgeInsets.init(top: 0, left: 5, bottom: 0, right: 0))
        navigationController?.navigationBar.backIndicatorImage = origImage
        navigationController?.navigationBar.backIndicatorTransitionMaskImage = origImage
        navigationController?.navigationBar.topItem?.title = " "
        navigationController?.navigationBar.tintColor = UIColor.white
    }
    
}
