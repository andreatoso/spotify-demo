//
//  CustomTabBarController.swift
//  Spotify-Demo
//
//  Created by Andrea Toso on 20/08/18.
//  Copyright © 2018 Andrea Toso. All rights reserved.
//

import UIKit

class CustomTabBarController: UITabBarController, UIGestureRecognizerDelegate {    
    
    let trackController = TrackController()
    var config = TrackControllerConfig()
    var displayLink = CADisplayLink()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupTabBarStyle()
        setupControllers()
        setupTrackController()
        setupGesture()
        setupActions()
    }
    
    fileprivate func setupTabBarStyle() {
        tabBar.isTranslucent = false
        tabBar.barTintColor = Color.Gray
        tabBar.unselectedItemTintColor = Color.LightGray
        tabBar.tintColor = UIColor.white
    }
    
    fileprivate func setupControllers() {
        let hc = HomeController(collectionViewLayout: UICollectionViewFlowLayout())
        hc.delegate = self
        hc.trackController = self.trackController
        let homeController = UINavigationController(rootViewController: hc)
        homeController.tabBarItem.title = "Home"
        homeController.tabBarItem.image = UIImage(named: "home")
        homeController.tabBarItem.selectedImage = UIImage(named: "home_selected")
        
        let browseController = UINavigationController(rootViewController: ArtistController(collectionViewLayout: UICollectionViewFlowLayout()))
        browseController.tabBarItem.title = "Naviga"
        browseController.tabBarItem.image = UIImage(named: "browse")
        
        let sc = SearchController(collectionViewLayout: UICollectionViewFlowLayout())
        sc.delegate = self
        sc.trackController = self.trackController
        let searchController = UINavigationController(rootViewController: sc)
        searchController.tabBarItem.title = "Ricerca"
        searchController.tabBarItem.image = UIImage(named: "search")
        
        let radioController = UINavigationController(rootViewController: UIViewController())
        radioController.tabBarItem.title = "Radio"
        radioController.tabBarItem.image = UIImage(named: "radio")
        
        let libraryController = UINavigationController(rootViewController: UIViewController())
        libraryController.tabBarItem.title = "La tua libreria"
        libraryController.tabBarItem.image = UIImage(named: "library")
        
        viewControllers = [homeController, browseController, searchController, radioController, libraryController]
    }
    
    fileprivate func setupTrackController() {
        trackController.translatesAutoresizingMaskIntoConstraints = false
        view.insertSubview(trackController, belowSubview: tabBar)
        trackController.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        trackController.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        trackController.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        trackController.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        
        let window = UIApplication.shared.keyWindow
        guard let bottomPadding = window?.safeAreaInsets.bottom else {return}
        let tabBarHeight = tabBar.frame.height
        let fixedBottomContainerHeight = config.fixedBottomContainerHeight
        
        config.closedTransform = CGAffineTransform(translationX: 0, y: view.bounds.height - tabBarHeight - bottomPadding - fixedBottomContainerHeight)
        trackController.transform = config.closedTransform
    }
    
    fileprivate func setupGesture() {
        config.panRecognier.delegate = self
        config.panRecognier.addTarget(self, action: #selector(panned))
        config.panRecognier.cancelsTouchesInView = false
        trackController.addGestureRecognizer(config.panRecognier)
        trackController.customTabBarController = self
        
        displayLink = CADisplayLink(target: self, selector: #selector(handleTrackControllerTransition(displayLink:)))
        displayLink.add(to: RunLoop.main, forMode: RunLoop.Mode.common)
        displayLink.isPaused = true
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        guard let touchView = touch.view else {return false}
        return !touchView.isKind(of: UIButton.self)
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
}
