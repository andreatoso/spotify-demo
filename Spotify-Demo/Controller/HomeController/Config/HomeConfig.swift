//
//  HomeControllerCVIDs.swift
//  Spotify-Demo
//
//  Created by Andrea Toso on 26/08/18.
//  Copyright © 2018 Andrea Toso. All rights reserved.
//

import Foundation

struct HomeConfig {
    
    let HEADER_ID = "HEADER_ID"
    let RECENTLY_PLAYED_CELL_ID = "RECENTLY_PLAYED_CELL_ID"
    let MADE_FOR_YOU_CELL_ID = "MADE_FOR_YOU_CELL_ID"
    let SUGGESTED_CELL_ID = "SUGGESTED_CELL_ID"
    
}
