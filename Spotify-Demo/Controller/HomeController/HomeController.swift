//
//  ViewController.swift
//  Spotify-Demo
//
//  Created by Andrea Toso on 20/08/18.
//  Copyright © 2018 Andrea Toso. All rights reserved.
//

import UIKit
protocol HomeControllerDelegate: class {
    func pushController(controller: UIViewController)
}

class HomeController: UICollectionViewController, UICollectionViewDelegateFlowLayout, HomeControllerDelegate {
    
    func pushController(controller: UIViewController) {
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    
    var statusBarBg: UIView?
    private var recentlyPlayed: [Track] = []
    private var madeForYouPlaylists: [Playlist] = []
    private var featuredPlaylists: [FeaturedPlaylist] = []
    
    weak var delegate: PlayerDelegate?
    var trackController: TrackController?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavigationBar()
        setupController()
        fetchData()
    }
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 3
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        case 1:
            return 1
        case 2:
            return featuredPlaylists.count
        default:
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch indexPath.section {
        case 0:
            return CGSize(width: view.frame.size.width, height: 180)
        case 1:
            let ratio = view.frame.size.width/3
            let coverHeight = view.frame.size.width - ratio
            let cvHeight: CGFloat = 150
            let spacing: CGFloat = 40.5 + 55 + 40
            return CGSize(width: view.frame.size.width, height: coverHeight + cvHeight + spacing)
        case 2:
            let squareRatio = view.frame.size.width/3
            return CGSize(width: view.frame.size.width, height: view.frame.size.width - squareRatio/2.6)
        default:
            return .zero
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch indexPath.section {
        case 0:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: HomeConfig().RECENTLY_PLAYED_CELL_ID, for: indexPath) as! HorizontalCV
            cell.setupHorizontalModel(model: self.recentlyPlayed)
            cell.delegate = self.delegate
            return cell
        case 1:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: HomeConfig().MADE_FOR_YOU_CELL_ID, for: indexPath) as! VerticalCell
            cell.requestHelperCv = true
            cell.setupMadeForYouPlaylists(model: self.madeForYouPlaylists)
            cell.delegate = self.delegate
            cell.homeControllerDelegate = self
            cell.trackController = self.trackController
            return cell
        case 2:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: HomeConfig().SUGGESTED_CELL_ID, for: indexPath) as! VerticalCell
            cell.requestHelperCv = false
            cell.featuredPlaylist = featuredPlaylists[indexPath.item]
            return cell
        default:
            return UICollectionViewCell()
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let libraryController = LibraryController(collectionViewLayout: UICollectionViewFlowLayout())
        libraryController.delegate = self.delegate
        libraryController.trackController = self.trackController
        
        switch indexPath.section {
        case 1:
            libraryController.library = Library(id: madeForYouPlaylists[0].id, type: .playlist)
            self.navigationController?.pushViewController(libraryController, animated: true)
        case 2:
            libraryController.library = Library(id: featuredPlaylists[indexPath.item].id, type: .playlist)
            self.navigationController?.pushViewController(libraryController, animated: true)
        default:
            break
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        if section+1 == collectionView.numberOfSections {
            return UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
        }
        return UIEdgeInsets.init(top: 0, left: 0, bottom: 60, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: view.frame.size.width, height: 45)
    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: HomeConfig().HEADER_ID, for: indexPath) as! SectionHeader
        
        let headerTitles = ["Recently played", "Made for you", "Recommended for you"]
        header.title.text = headerTitles[indexPath.section]
        
        return header
    }
    
}

extension HomeController {
    
    internal func fetchData() {
        Service.sharedInstance.spotifyService.getRecentlyPlayed { (models) in
            self.recentlyPlayed = models
            self.updateDatasource()
        }
        Service.sharedInstance.spotifyService.getDailyMix { (playlists) in
            self.madeForYouPlaylists = playlists
            self.updateDatasource()
        }
        Service.sharedInstance.spotifyService.getFeaturedPlaylists { (playlists) in
            self.featuredPlaylists = playlists
            self.updateDatasource()
        }
    }
    
    internal func updateDatasource() {
        DispatchQueue.main.async {
            self.collectionView?.reloadData()
        }
    }
    
}
