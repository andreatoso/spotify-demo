//
//  HomeControllerScrollView.swift
//  Spotify-Demo
//
//  Created by Andrea Toso on 03/09/18.
//  Copyright © 2018 Andrea Toso. All rights reserved.
//

import UIKit

// MARK: - Scrollview
extension HomeController {
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        handleStatusNavAlpha(offset: scrollView.contentOffset.y)
    }
    
    fileprivate func handleStatusNavAlpha(offset: CGFloat) {
        self.navigationController?.navigationBar.subviews.forEach({ (subview) in
            subview.alpha = -offset/72
        })
        guard let statusBar = statusBarBg else {return}
        statusBar.backgroundColor = UIColor.black.withAlphaComponent(offset/20)
        statusBar.alpha = 0.5
    }
    
}
