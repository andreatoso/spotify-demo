//
//  HomeControllerSetup.swift
//  Spotify-Demo
//
//  Created by Andrea Toso on 26/08/18.
//  Copyright © 2018 Andrea Toso. All rights reserved.
//

import UIKit

// MARK: - Setup
extension HomeController {
    
    internal func setupNavigationBar() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        
        let button = UIButton()
        button.setImage(UIImage(named: "settings")?.withRenderingMode(.alwaysOriginal), for: .normal)
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: button)
    }
    
    fileprivate func setupHeader() {
        collectionView?.register(SectionHeader.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: HomeConfig().HEADER_ID)
    }
    
    fileprivate func setupCells() {
        collectionView?.register(HorizontalCV.self, forCellWithReuseIdentifier: HomeConfig().RECENTLY_PLAYED_CELL_ID)
        collectionView?.register(VerticalCell.self, forCellWithReuseIdentifier: HomeConfig().MADE_FOR_YOU_CELL_ID)
        collectionView?.register(VerticalCell.self, forCellWithReuseIdentifier: HomeConfig().SUGGESTED_CELL_ID)
    }
    
    internal func setupController() {
        collectionView?.backgroundColor = Color.Black
        collectionView?.contentInset = UIEdgeInsets.init(top: 8, left: 0, bottom: 0, right: 0)
        
        setupHeader()
        setupCells()
        
        statusBarBg = UIView()
        guard let statusBar = statusBarBg else {return}
        view.addSubview(statusBar)
        statusBar.anchor(view.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: UIApplication.shared.statusBarFrame.height)
    }
    
}
