//
//  LibraryConfig.swift
//  Spotify-Demo
//
//  Created by Andrea Toso on 11/09/18.
//  Copyright © 2018 Andrea Toso. All rights reserved.
//

import Foundation

struct LibraryConfig {
    let HEADER_ID = "HEADER_ID"
    let TRACK_ID = "TRACK_ID"
}
