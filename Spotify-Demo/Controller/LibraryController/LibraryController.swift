//
//  LibraryController.swift
//  Spotify-Demo
//
//  Created by Andrea Toso on 11/09/18.
//  Copyright © 2018 Andrea Toso. All rights reserved.
//

import UIKit

protocol LibraryControllerDelegate: class {
    func updateCurrentTrack(trackNumber: Int)
}

extension LibraryController: LibraryControllerDelegate {
    
    func updateCurrentTrack(trackNumber: Int) {
        selectedTrack = IndexPath(item: trackNumber, section: 0)
    }
    
}

class LibraryController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    var backgroundColorEffectConstraint: NSLayoutConstraint?
    var gradientViewConstraint: NSLayoutConstraint?
    var shuffleButtonConstraint: NSLayoutConstraint?
    var isScrollViewScrolling = false
    
    var library: Library?
    var tracks: [Track] = []
    var selectedTrack = IndexPath() {didSet {collectionView.reloadData()}}
    
    weak var delegate: PlayerDelegate?
    var trackController: TrackController? {
        didSet {
            trackController?.trackCV.libraryControllerDelagte = self
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupController()
        setupNavigationBar()
        setupViews()
    }
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tracks.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.size.width, height: 50)
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: LibraryConfig().TRACK_ID, for: indexPath) as! LibraryCell
        cell.setupLibraryTrack(model: tracks[indexPath.item])
        cell.songNumber.text = "\(indexPath.item+1)"
        if indexPath == selectedTrack {
            cell.name.textColor = Color.Green
        } else {
            cell.name.textColor = UIColor.white
        }
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.presentPlayer(for: tracks, at: indexPath)
        self.selectedTrack = indexPath
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 15
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: 5, left: 0, bottom: 0, right: 0)
    }
    
    let titleView: UILabel = {
        let title = UILabel()
        title.text = "Discover Weekly"
        title.textColor = UIColor.white
        title.font = UIFont.init(name: Font.Bold, size: 16)
        title.isHidden = true
        title.translatesAutoresizingMaskIntoConstraints = false
        return title
    }()
    
    let artistView: ArtistView = {
        let artistView = ArtistView()
        artistView.profileImage.layer.cornerRadius = 0
        artistView.shadow.isHidden = false
        artistView.translatesAutoresizingMaskIntoConstraints = false
        return artistView
    }()
    
    lazy var artistViewCopy: ArtistView = {
        let artistView = ArtistView()
        artistView.profileImage.layer.cornerRadius = 0
        artistView.shadow.isHidden = false
        artistView.layer.zPosition = -1
        artistView.isHidden = true
        artistView.isUserInteractionEnabled = false
        artistView.translatesAutoresizingMaskIntoConstraints = false
        return artistView
    }()
    
    let backgroundColorEffect: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.black
        view.layer.zPosition = -1
        view.isUserInteractionEnabled = false
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let gradientView: UIImageView = {
        let iv = UIImageView()
        iv.isUserInteractionEnabled = false
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    let shuffleButton: SpotifyButton = {
        let button = SpotifyButton()
        button.setTitle("PLAY", for: .normal)
        button.setTitleColor(UIColor.white, for: .normal)
        button.addTarget(self, action: #selector(startShuffle), for: .touchUpInside)
        button.layer.zPosition = 9999
        return button
    }()
    
    let shuffleGradient: UIImageView = {
        let iv = UIImageView()
        iv.isHidden = true
        iv.isUserInteractionEnabled = false
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fetchData()
    }
    
    @objc fileprivate func startShuffle() {
        delegate?.presentPlayer(for: tracks, at: IndexPath(item: 0, section: 0))
        selectedTrack = IndexPath(item: 0, section: 0)
    }
    
}

extension LibraryController {
    
    internal func fetchData() {
        guard let library = library else {return}
        switch library.type {
        case .album:
            Service.sharedInstance.spotifyService.getAlbum(id: library.id) { (album) in
                self.tracks = album.tracks
                for index in 0...self.tracks.count-1 {
                    self.tracks[index].album = album
                }
                DispatchQueue.main.async {
                    self.collectionView?.reloadData()
                    self.setupAlbumLibrary(for: album)
                    self.titleView.text = album.name
                    self.navigationItem.titleView = self.titleView
                }
            }
        case .playlist:
            Service.sharedInstance.spotifyService.getPlaylist(id: library.id) { (playlist) in
                self.tracks = playlist.tracks
                DispatchQueue.main.async {
                    self.collectionView?.reloadData()
                    self.setupPlaylistLibrary(for: playlist)
                    self.titleView.text = playlist.name
                    self.navigationItem.titleView = self.titleView
                }
            }
        default:
            break
        }
    }
    
    fileprivate func setupAlbumLibrary(for album: Album) {
        artistView.name.text = album.name
        artistView.profileImage.loadImage(urlString: album.imageUrl)
        if let owner = album.artists.first {
            artistView.followers.text = "Album by \(owner.name)"
            artistView.formatFollowersStyle(for: .album)
        }
        setupArtistViewCopy()
        setAvarageColor()
        titleView.text = album.name
    }
    
    fileprivate func setupPlaylistLibrary(for playlist: Playlist) {
        artistView.name.text = playlist.name
        artistView.profileImage.loadImage(urlString: playlist.imageUrl)
        let followers = "\(playlist.followers)".decimalFormat
        artistView.followers.text = "\(followers) FOLLOWERS"
        artistView.formatFollowersStyle(for: .playlist)
        setupArtistViewCopy()
        setAvarageColor()
    }
    
    fileprivate func setupArtistViewCopy() {
        if let text = self.artistView.name.text, let followers = self.artistView.followers.text {
            artistViewCopy.name.text = text
            artistViewCopy.followers.text = followers
            artistViewCopy.formatFollowersStyle(for: .album)
        }
        artistViewCopy.profileImage.image = self.artistView.profileImage.image
    }
    
    fileprivate func setAvarageColor() {
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.5, animations: {
                self.backgroundColorEffect.backgroundColor = self.artistView.profileImage.image?.getAvarageColor()
            })
        }
    }
    
}
