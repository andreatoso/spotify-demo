//
//  LibraryControllerScrollView.swift
//  Spotify-Demo
//
//  Created by Andrea Toso on 11/09/18.
//  Copyright © 2018 Andrea Toso. All rights reserved.
//

import UIKit

// MARK: - Scrollview
extension LibraryController {
    
    override func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        isScrollViewScrolling = true
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        handleTitleViewAlpha(offset: scrollView.contentOffset.y)
        handleScrollEffect(offset: scrollView.contentOffset.y)
        handleShuffleButton(offset: scrollView.contentOffset.y)
    }
    
    // MARK: - NavigationBar TitleView
    fileprivate func handleTitleViewAlpha(offset: CGFloat) {
        guard let navBarHeight = navigationController?.navigationBar.frame.size.height else {return}
        let statusBarHeight = UIApplication.shared.statusBarFrame.size.height
        let totalHeight = navBarHeight + statusBarHeight
        
        if offset >= -265 - totalHeight {
            let difference: CGFloat = -offset-(175+totalHeight)
            let alpha = -(difference/((175+totalHeight)-(140+totalHeight)))
            navigationItem.titleView?.alpha = alpha
            titleView.isHidden = false
        } else {
            titleView.isHidden = true
        }
    }
    
    // MARK: - ArtistView
    fileprivate func handleScrollEffect(offset: CGFloat) {
        guard let navBarHeight = navigationController?.navigationBar.frame.size.height else {return}
        let statusBarHeight = UIApplication.shared.statusBarFrame.size.height
        let totalHeight = navBarHeight + statusBarHeight
        
        if offset >= -250 - totalHeight {
            artistView.isHidden = true
            artistViewCopy.isHidden = false
            
            artistViewCopy.alpha = (-offset/30)/10
            
            let y1: CGFloat = 0.85
            let y2: CGFloat = 1.0
            let x1: CGFloat = 0.0
            let x2: CGFloat = 250.0 + totalHeight
            
            let m: CGFloat = (y2-y1)/(x2-x1)
            let q: CGFloat = 1-(((y2-y1)/(x2-x1))*x2)
            let y = m*(-offset)+q
            artistViewCopy.transform = CGAffineTransform(scaleX: y, y: y)
        } else {
            artistView.isHidden = false
            artistViewCopy.isHidden = true
        }
    }
    
    // MARK: - Shuffle Button
    fileprivate func handleShuffleButton(offset: CGFloat) {
        guard let navBarHeight = navigationController?.navigationBar.frame.size.height else {return}
        let statusBarHeight = UIApplication.shared.statusBarFrame.size.height
        let totalHeight = navBarHeight + statusBarHeight
        
        if offset >= -(totalHeight+25) {
            pinShuffleButton(isPin: true)
            handleGradientConstraints(isGradientFixed: true)
        } else {
            if isScrollViewScrolling {
                pinShuffleButton(isPin: false)
                handleGradientConstraints(isGradientFixed: false)
            }
        }
    }
    
    // MARK: - Gradient view
    fileprivate func handleGradientConstraints(isGradientFixed: Bool) {
        if isGradientFixed {
            setConstraint(customView: backgroundColorEffect, current: &backgroundColorEffectConstraint, equalTo: shuffleButton.centerYAnchor)
            setConstraint(customView: gradientView, current: &gradientViewConstraint, equalTo: shuffleButton.centerYAnchor)
            
            shuffleGradient.isHidden = false
            backgroundColorEffect.layer.zPosition = 9999
        } else {
            guard let topAnchor = collectionView?.topAnchor else {return}
            setConstraint(customView: backgroundColorEffect, current: &backgroundColorEffectConstraint, equalTo: topAnchor)
            setConstraint(customView: gradientView, current: &gradientViewConstraint, equalTo: topAnchor)
            
            shuffleGradient.isHidden = true
            backgroundColorEffect.layer.zPosition = -1
        }
    }
    
    fileprivate func pinShuffleButton(isPin: Bool) {
        guard let topAnchor = collectionView?.topAnchor else {return}
        guard let navBarHeight = navigationController?.navigationBar.frame.size.height else {return}
        let statusBarHeight = UIApplication.shared.statusBarFrame.size.height
        let totalHeight = navBarHeight + statusBarHeight
        shuffleButtonConstraint?.isActive = false
        isPin ? (shuffleButtonConstraint = shuffleButton.topAnchor.constraint(equalTo: view.topAnchor, constant: totalHeight)) : (shuffleButtonConstraint = shuffleButton.topAnchor.constraint(equalTo: topAnchor, constant: -25))
        shuffleButtonConstraint?.isActive = true
    }
    
    fileprivate func setConstraint(customView: UIView, current: inout NSLayoutConstraint?, equalTo newConstraint: NSLayoutAnchor<NSLayoutYAxisAnchor>) {
        current?.isActive = false
        current = customView.bottomAnchor.constraint(equalTo: newConstraint)
        current?.isActive = true
    }
    
}
