//
//  LibraryControllerSetup.swift
//  Spotify-Demo
//
//  Created by Andrea Toso on 11/09/18.
//  Copyright © 2018 Andrea Toso. All rights reserved.
//

import UIKit

// MARK: - Setup
extension LibraryController {
    
    fileprivate func setupCells() {
        collectionView?.register(LibraryCell.self, forCellWithReuseIdentifier: LibraryConfig().TRACK_ID)
    }
    
    internal func setupController() {
        collectionView?.backgroundColor = Color.Black
        collectionView?.showsVerticalScrollIndicator = false
        collectionView?.contentInset = UIEdgeInsets.init(top: 270, left: 0, bottom: 15, right: 0)
        
        setupHeader()
        setupCells()
    }
    
}

// MARK: - Header
extension LibraryController {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: view.frame.size.width, height: 45 + 53)
    }

    fileprivate func setupHeader() {
        collectionView?.register(LibraryCellHeader.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: LibraryConfig().HEADER_ID)
    }
    
}

// MARK: - Setup Header
extension LibraryController {
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: LibraryConfig().HEADER_ID, for: indexPath) as! LibraryCellHeader
        return header
    }
    
}

