//
//  SearchConfig.swift
//  Spotify-Demo
//
//  Created by Andrea Toso on 26/08/18.
//  Copyright © 2018 Andrea Toso. All rights reserved.
//

import Foundation
import UIKit

struct SearchConfig {
    
    let FIRST_HEADER_ID = "FIRST_HEADER_ID"
    let HEADER_ID = "HEADER_ID"
    let FOOTER_ID = "FOOTER_ID"
    let RELEVANT_RESULT_ID = "RELEVANT_RESULT_ID"
    let ARTIST_RESULT_ID = "ARTIST_RESULT_ID"
    let TRACK_RESULT_ID = "TRACK_RESULT_ID"
    let PLAYLIST_RESULT_ID = "PLAYLIST_RESULT_ID"
    let ALBUM_RESULT_ID = "ALBUM_RESULT_ID"
    var CV_INITIAL_SCROLL_OFFSET: CGFloat = 0
    
}
