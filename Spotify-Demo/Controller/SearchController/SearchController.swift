//
//  SearchController.swift
//  Spotify-Demo
//
//  Created by Andrea Toso on 22/08/18.
//  Copyright © 2018 Andrea Toso. All rights reserved.
//

import UIKit

class SearchController: UICollectionViewController, UICollectionViewDelegateFlowLayout, UISearchBarDelegate, UITextFieldDelegate {
    
    var welcomeConstraint: NSLayoutConstraint?
    var result: SearchResult?
    var config = SearchConfig()
    
    weak var delegate: PlayerDelegate?
    var trackController: TrackController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupController()
        setupViews()
        setupNavigationBar()
    }
    
    let navBarBackground: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(r: 26, g: 27, b: 28, a: 1)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let navBarContainer: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.clear
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let navBarTextfieldContainer: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(r: 41, g: 42, b: 43, a: 1)
        view.layer.cornerRadius = 5
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var dismissButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Annulla", for: .normal)
        button.setTitleColor(UIColor.white, for: .normal)
        button.titleLabel?.font = UIFont.init(name: Font.Book, size: 14)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(dismissKeyboard), for: .touchUpInside)
        return button
    }()
    
    lazy var searchTextField: UITextField = {
        let tf = UITextField()
        tf.placeholder = "Cerca"
        tf.textColor = UIColor.white
        tf.backgroundColor = UIColor.clear
        tf.tintColor = Color.Green
        tf.font = UIFont.init(name: Font.Book, size: 13)
        tf.attributedPlaceholder = NSAttributedString(string: "Cerca", attributes: [
            .foregroundColor: UIColor.white,
            .font: UIFont.init(name: Font.Book, size: 13) as Any
            ])
        tf.keyboardAppearance = .dark
        tf.autocorrectionType = .no
        tf.returnKeyType = .search
        tf.delegate = self
        tf.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        tf.addTarget(self, action: #selector(setupKeyboardObservers), for: .editingDidBegin)
        tf.translatesAutoresizingMaskIntoConstraints = false
        return tf
    }()
    
    let searchIconTextField: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "search")?.withRenderingMode(.alwaysTemplate)
        iv.tintColor = UIColor.white
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    lazy var cancelIcon: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "cancel")?.withRenderingMode(.alwaysTemplate)
        iv.tintColor = UIColor.white
        iv.isHidden = true
        iv.isUserInteractionEnabled = true
        iv.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(clearTextField)))
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    let searchIcon: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "searchBig")?.withRenderingMode(.alwaysTemplate)
        iv.tintColor = UIColor.white
        iv.alpha = 0.6
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    let welcomeContainer: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let welcomeLabel: UILabel = {
        let label = UILabel()
        label.text = "Trova la musica che più ti piace"
        label.textColor = UIColor.white
        label.font = UIFont.init(name: Font.Book, size: 16.5)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let welcomeDesc: UILabel = {
        let label = UILabel()
        label.text = "Cerca artisti, brani, playlist e\naltro ancora,"
        label.textColor = Color.LightGray
        label.font = UIFont.init(name: Font.Book, size: 13.5)
        label.numberOfLines = 2
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        _ = result == nil ? searchTextField.becomeFirstResponder() : nil
        if config.CV_INITIAL_SCROLL_OFFSET == 0 {
            config.CV_INITIAL_SCROLL_OFFSET = collectionView.contentOffset.y
        }
    }
    
}

extension SearchController {
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 5
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let result = result else {return 0}
        switch section {
        case 0:
            return (result.relevantResult == nil && result.relevantResult == nil) ? 0 : 1
        case 1:
            return result.artists.count > 5 ? 5 : result.artists.count
        case 2:
            return result.tracks.count > 5 ? 5 : result.tracks.count
        case 3:
            return result.playlists.count > 5 ? 5 : result.playlists.count
        case 4:
            return result.albums.count > 5 ? 5 : result.albums.count
        default:
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.size.width, height: 50)
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let result = result else {return UICollectionViewCell()}
        switch indexPath.section {
        case 0:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: config.RELEVANT_RESULT_ID, for: indexPath) as! SearchRelevantCell
            cell.setupSearchResult(model: result.relevantResult)
            return cell
        case 1:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: config.ARTIST_RESULT_ID, for: indexPath) as! SearchStandardCell
            cell.setupSearchArtistResult(model: result.artists[indexPath.item])
            return cell
        case 2:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: config.TRACK_RESULT_ID, for: indexPath) as! SearchTrackCell
            cell.setupTracksResult(model: result.tracks[indexPath.item])
            return cell
        case 3:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: config.PLAYLIST_RESULT_ID, for: indexPath) as! SearchStandardCell
            cell.setupSearchPlaylistResult(model: result.playlists[indexPath.item])
            cell.profileImage.layer.cornerRadius = 0
            return cell
        case 4:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: config.ALBUM_RESULT_ID, for: indexPath) as! SearchAlbumCell
            cell.setupAlbumResult(model: result.albums[indexPath.item])
            return cell
        default:
            return UICollectionViewCell()
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let result = result else {return}
        let libraryController = LibraryController(collectionViewLayout: UICollectionViewFlowLayout())
        libraryController.delegate = self.delegate
        libraryController.trackController = self.trackController
        
        let artistController = ArtistController(collectionViewLayout: UICollectionViewFlowLayout())
        artistController.delegate = self.delegate
        artistController.trackController = self.trackController
        
        switch indexPath.section {
        case 0:
            guard let result = result.relevantResult else {return}
            switch result.type {
            case .track:
                Service.sharedInstance.spotifyService.getTrack(id: result.id) { (track) in
                    self.delegate?.presentPlayer(for: [track], at: IndexPath(item: 0, section: 0))
                }
            case .artist:
                Service.sharedInstance.spotifyService.getArtistProfile(id: result.id) { (artist) in
                    artistController.artist = artist
                    self.navigationController?.pushViewController(artistController, animated: true)
                }
            default:
                break
            }
        case 1:
            artistController.artist = result.artists[indexPath.item]
            self.navigationController?.pushViewController(artistController, animated: true)
        case 2:
            delegate?.presentPlayer(for: [result.tracks[indexPath.item]], at: IndexPath(item: 0, section: 0))
        case 3:
            libraryController.library = Library(id: result.playlists[indexPath.item].id, type: .playlist)
            self.navigationController?.pushViewController(libraryController, animated: true)
        case 4:
            libraryController.library = Library(id: result.albums[indexPath.item].id, type: .album)
            self.navigationController?.pushViewController(libraryController, animated: true)
        default:
            break
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 15
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: 5, left: 0, bottom: 0, right: 0)
    }
    
}
