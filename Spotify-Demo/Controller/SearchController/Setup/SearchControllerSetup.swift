//
//  SearchControllerSetup.swift
//  Spotify-Demo
//
//  Created by Andrea Toso on 26/08/18.
//  Copyright © 2018 Andrea Toso. All rights reserved.
//

import UIKit

// MARK: - Setup
extension SearchController {
    
    fileprivate func setupCells() {
        collectionView?.register(SearchRelevantCell.self, forCellWithReuseIdentifier: config.RELEVANT_RESULT_ID)
        collectionView?.register(SearchStandardCell.self, forCellWithReuseIdentifier: config.ARTIST_RESULT_ID)
        collectionView?.register(SearchTrackCell.self, forCellWithReuseIdentifier: config.TRACK_RESULT_ID)
        collectionView?.register(SearchStandardCell.self, forCellWithReuseIdentifier: config.PLAYLIST_RESULT_ID)
        collectionView?.register(SearchAlbumCell.self, forCellWithReuseIdentifier: config.ALBUM_RESULT_ID)
    }
    
    internal func setupController() {
        collectionView?.contentInset = UIEdgeInsets.init(top: 10, left: 0, bottom: 15 + 30, right: 0)
        collectionView?.isHidden = true
        collectionView?.backgroundColor = Color.Black
        view.backgroundColor = Color.Black
        
        setupHeader()
        setupFooter()
        setupCells()
    }
    
}

// MARK: - Header
extension SearchController {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        if collectionView.numberOfItems(inSection: section) == 0 {
            return .zero
        }
        if section == 0 {
            return CGSize(width: view.frame.size.width, height: 45)
        } else {
            return CGSize(width: view.frame.size.width, height: 45 + 20)
        }
    }
    
    fileprivate func setupHeader() {
        collectionView?.register(SectionHeader.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: config.HEADER_ID)
        collectionView?.register(SectionHeader.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: config.FIRST_HEADER_ID)
    }
    
}

// MARK: - Footer
extension SearchController {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        if section != 0 {
            if collectionView.numberOfItems(inSection: section) == 0 {
                return .zero
            }
            return CGSize(width: view.frame.size.width, height: 45 + 14)
        }
        return .zero
    }
    
    fileprivate func setupFooter() {
        collectionView?.register(SearchCellFooter.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: config.FOOTER_ID)
    }
    
}

// MARK: - Setup Header and Footer
extension SearchController {
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if kind == UICollectionView.elementKindSectionHeader {
            var ids = [config.FIRST_HEADER_ID]
            for _ in 0...collectionView.numberOfSections-2 {ids.append(config.HEADER_ID)}
            
            let headerTitles = ["Risultato più rilevante", "Artisti", "Brani", "Playlist", "Album"]
            let header = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: ids[indexPath.section], for: indexPath) as! SectionHeader
            header.title.text = headerTitles[indexPath.section]
            return header
        } else {
            let footer = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: config.FOOTER_ID, for: indexPath) as! SearchCellFooter
            return footer
        }
        
    }
    
}
