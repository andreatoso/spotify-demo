//
//  SearchControllerSetup.swift
//  Spotify-Demo
//
//  Created by Andrea Toso on 24/08/18.
//  Copyright © 2018 Andrea Toso. All rights reserved.
//

import UIKit

// MARK: - Setup
extension SearchController {
    
    internal func setupNavigationBar() {
        guard let navbar = navigationController?.navigationBar else {return}
        
        let statusBarHeight = UIApplication.shared.statusBarFrame.height
        view.addSubview(navBarBackground)
        navBarBackground.anchor(view.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: statusBarHeight + navbar.frame.height)
        
        navBarContainer.heightAnchor.constraint(equalToConstant: navbar.frame.height).isActive = true
        navBarContainer.widthAnchor.constraint(equalToConstant: navbar.frame.width - 12 - 12 - 8).isActive = true

        navBarContainer.addSubview(dismissButton)
        dismissButton.anchorCenterYToSuperview()
        dismissButton.leftAnchor.constraint(equalTo: navBarContainer.leftAnchor, constant: UIScreen.main.bounds.width - 48 - 35).isActive = true

        navbar.setBackgroundImage(UIImage(), for: .default)
        navbar.backgroundColor = .clear
        navBarContainer.addSubview(navBarTextfieldContainer)
        navBarTextfieldContainer.anchor(navBarContainer.topAnchor, left: navBarContainer.leftAnchor, bottom: navBarContainer.bottomAnchor, right: dismissButton.leftAnchor, topConstant: 8, leftConstant: 0, bottomConstant: 8, rightConstant: 12, widthConstant: 0, heightConstant: 0)

        navBarTextfieldContainer.addSubview(searchIconTextField)
        searchIconTextField.anchorCenterYToSuperview()
        searchIconTextField.leftAnchor.constraint(equalTo: navBarTextfieldContainer.leftAnchor, constant: 8).isActive = true
        searchIconTextField.heightAnchor.constraint(equalToConstant: 13).isActive = true
        searchIconTextField.widthAnchor.constraint(equalToConstant: 13).isActive = true

        navBarTextfieldContainer.addSubview(cancelIcon)
        cancelIcon.anchorCenterYToSuperview()
        cancelIcon.rightAnchor.constraint(equalTo: navBarTextfieldContainer.rightAnchor, constant: -8).isActive = true
        cancelIcon.heightAnchor.constraint(equalToConstant: 12).isActive = true
        cancelIcon.widthAnchor.constraint(equalToConstant: 12).isActive = true

        navBarTextfieldContainer.addSubview(searchTextField)
        searchTextField.anchor(navBarTextfieldContainer.topAnchor, left: searchIconTextField.rightAnchor, bottom: navBarTextfieldContainer.bottomAnchor, right: cancelIcon.leftAnchor, topConstant: 0, leftConstant: 9, bottomConstant: 0, rightConstant: 8, widthConstant: 0, heightConstant: 0)

        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: navBarContainer)
    }
    
    internal func setupViews() {
        guard let navbar = navigationController?.navigationBar else {return}
        let padding = navbar.frame.height + UIApplication.shared.statusBarFrame.height
        view.addSubview(welcomeContainer)
        welcomeContainer.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        welcomeContainer.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        welcomeContainer.topAnchor.constraint(equalTo: view.topAnchor, constant: padding).isActive = true
        
        welcomeConstraint = welcomeContainer.heightAnchor.constraint(equalToConstant: view.frame.height)
        welcomeConstraint?.isActive = true
        
        welcomeContainer.addSubview(welcomeLabel)
        welcomeLabel.centerYAnchor.constraint(equalTo: welcomeContainer.centerYAnchor).isActive = true
        welcomeLabel.centerXAnchor.constraint(equalTo: welcomeContainer.centerXAnchor).isActive = true
        
        welcomeContainer.addSubview(searchIcon)
        searchIcon.anchorCenterXToSuperview()
        searchIcon.anchor(nil, left: nil, bottom: welcomeLabel.topAnchor, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 20, rightConstant: 0, widthConstant: 70, heightConstant: 70)
        
        welcomeContainer.addSubview(welcomeDesc)
        welcomeDesc.anchorCenterXToSuperview()
        welcomeDesc.topAnchor.constraint(equalTo: welcomeLabel.bottomAnchor, constant: 20).isActive = true
    }
    
}
