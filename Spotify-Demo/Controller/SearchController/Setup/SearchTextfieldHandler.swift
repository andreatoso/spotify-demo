//
//  SearchTextfieldHandler.swift
//  Spotify-Demo
//
//  Created by Andrea Toso on 24/08/18.
//  Copyright © 2018 Andrea Toso. All rights reserved.
//

import UIKit

// MARK: - Keyboard
extension SearchController {
    
    @objc internal func setupKeyboardObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc fileprivate func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            handleWelcomeWithKeyboard(keyboardSize: keyboardSize, isKeyboardOpen: true)
            self.welcomeContainer.isHidden = false
            self.collectionView?.isHidden = true
        }
    }
    
    @objc fileprivate func keyboardWillHide(notification: NSNotification) {
        handleWelcomeWithKeyboard(isKeyboardOpen: false)
    }
    
    fileprivate func handleWelcomeWithKeyboard(keyboardSize: CGRect = .zero, isKeyboardOpen: Bool) {
        guard let tabBar = tabBarController?.tabBar else {return}
        self.welcomeConstraint?.isActive = false
        let calculateHeight = view.frame.height - keyboardSize.height
        let constant = isKeyboardOpen ? (calculateHeight) : (view.frame.height - tabBar.frame.height)
        self.welcomeConstraint = self.welcomeContainer.heightAnchor.constraint(equalToConstant: constant)
        self.welcomeConstraint?.isActive = true
        self.view.layoutIfNeeded()
    }
    
    @objc internal func dismissKeyboard() {
        view.endEditing(true)
        searchTextField.resignFirstResponder()
    }
    
    @objc internal func clearTextField() {
        self.searchTextField.becomeFirstResponder()
        searchTextField.text = ""
        cancelIcon.isHidden = true
        welcomeContainer.isHidden = false
        collectionView?.isHidden = true
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        guard let text = textField.text else {return}
        cancelIcon.isHidden = text.isEmpty
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        guard let query = textField.text else {return true}
        self.collectionView.setContentOffset(CGPoint(x: 0, y: config.CV_INITIAL_SCROLL_OFFSET), animated: false)
        Service.sharedInstance.spotifyService.search(query: query) { (result) in
            self.result = result
            
            self.searchTextField.resignFirstResponder()
            self.welcomeContainer.isHidden = true
            self.collectionView?.isHidden = false
            self.collectionView?.reloadData()
        }
        
        return true
    }
}
