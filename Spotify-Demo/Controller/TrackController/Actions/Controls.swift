//
//  Controls.swift
//  Spotify-Demo
//
//  Created by Andrea Toso on 13/10/2018.
//  Copyright © 2018 Andrea Toso. All rights reserved.
//

import UIKit

extension TrackController {
    
    @objc func playerDidFinishPlaying(note: NSNotification){
        playNextTrack()
    }
    
    @objc internal func playNextTrack() {
        let nextIndex = indexPath.item+1
        if nextIndex < tracks.count {
            indexPath = IndexPath(item: nextIndex, section: indexPath.section)
        }
    }
    
    @objc internal func playPreviousTrack() {
        let nextIndex = indexPath.item-1
        if nextIndex < tracks.count && nextIndex >= 0 {
            indexPath = IndexPath(item: nextIndex, section: indexPath.section)
        }
    }
    
    @objc internal func controlTrack() {
        if let status = player?.timeControlStatus {
            if status == .playing {
                player?.pause()
                controls.playButton.setImage(UIImage(named: "play_button")?.withRenderingMode(.alwaysTemplate), for: .normal)
                playButtonFixed.setImage(UIImage(named: "play_circle")?.withRenderingMode(.alwaysTemplate), for: .normal)
            } else {
                player?.play()
                controls.playButton.setImage(UIImage(named: "pause_button")?.withRenderingMode(.alwaysTemplate), for: .normal)
                playButtonFixed.setImage(UIImage(named: "pause_circle")?.withRenderingMode(.alwaysTemplate), for: .normal)
            }
        }
    }
    
}
