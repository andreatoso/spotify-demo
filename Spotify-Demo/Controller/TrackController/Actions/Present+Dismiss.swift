//
//  Present+Dismiss.swift
//  Spotify-Demo
//
//  Created by Andrea Toso on 13/10/2018.
//  Copyright © 2018 Andrea Toso. All rights reserved.
//

import Foundation

protocol PlayerDelegate: class {
    func presentPlayer(for tracks: [Track], at index: IndexPath)
}

extension CustomTabBarController: PlayerDelegate {
    
    func presentPlayer(for tracks: [Track], at index: IndexPath) {
        trackController.tracks = tracks
        trackController.indexPath = index
        displayLink.isPaused = false
        startAnimationIfNeeded()
    }
    
    @objc func dismissPlayer() {
        displayLink.isPaused = false
        beganState()
        if !config.isMoving {
            self.trackController.isUserInteractionEnabled = false
            self.config.isMoving = true
        }
        config.animator.continueAnimation(withTimingParameters: nil, durationFactor: 0)
    }
    
}
