//
//  TrackControllerActions.swift
//  Spotify-Demo
//
//  Created by Andrea Toso on 15/09/18.
//  Copyright © 2018 Andrea Toso. All rights reserved.
//

import Foundation

extension CustomTabBarController {
    
    internal func setupActions() {
        trackController.header.closePlayerButton.addTarget(self, action: #selector(dismissPlayer), for: .touchUpInside)
    }
    
}
