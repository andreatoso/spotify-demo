//
//  TrackControllerConfig.swift
//  Spotify-Demo
//
//  Created by Andrea Toso on 14/09/18.
//  Copyright © 2018 Andrea Toso. All rights reserved.
//

import UIKit

struct TrackControllerConfig {
    var panRecognier = InstantPanGestureRecognizer()
    var animator = UIViewPropertyAnimator()
    var animationProgress: CGFloat = 0
    var closedTransform = CGAffineTransform.identity
    var isOpen = false
    var isMoving = false
    let fixedBottomContainerHeight: CGFloat = 46
}
