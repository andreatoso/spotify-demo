//
//  TrackInfo+.swift
//  Spotify-Demo
//
//  Created by Andrea Toso on 13/10/2018.
//  Copyright © 2018 Andrea Toso. All rights reserved.
//

import Foundation
import UIKit

extension TrackController {
    
    internal func setupInfo(track: Track) {
        header.source.text = track.album.name
        name.trackName.text = track.name
        name.trackArtist.text = getArtists(for: track)
        
        duration.slider.value = 0
        duration.startDuration.text = "00:00"
        duration.endDuration.text = "00:00"
        
        var artist = ""
        if !track.artists.isEmpty {artist = track.artists[0].name}
        let trackString = NSMutableAttributedString(string: "\(track.name) • ", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        let artistString = NSAttributedString(string: "\(artist)", attributes: [NSAttributedString.Key.foregroundColor: Color.LightGray])
        trackString.append(artistString)
        trackNameFixed.attributedText = trackString
    }
    
    fileprivate func getArtists(for track: Track) -> String {
        var artistStringText = ""
        track.artists.forEach({artistStringText += "\($0.name), "})
        
        //Remove ", " characters
        if !artistStringText.isEmpty {
            artistStringText.remove(at: artistStringText.index(before: artistStringText.endIndex))
            artistStringText.remove(at: artistStringText.index(before: artistStringText.endIndex))
            return artistStringText
        } else {
            return ""
        }
    }
    
}
