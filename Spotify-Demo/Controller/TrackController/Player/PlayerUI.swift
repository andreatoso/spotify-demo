//
//  PlayerUI.swift
//  Spotify-Demo
//
//  Created by Andrea Toso on 13/10/2018.
//  Copyright © 2018 Andrea Toso. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation

extension TrackController {
    
    internal func setupUI(for track: Track) {
        self.isHidden = false
        handleBackground(for: track)
    }
    
    fileprivate func handleBackground(for track: Track) {
        if self.currentTrack == nil {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                self.setAvarageColor(for: track)
            })
        } else {
            if self.currentTrack?.album.id != track.album.id {
                self.playerContainerOverlayColor.backgroundColor = .clear
                self.playerContainerImageView.image = nil
                self.playerContainerImageView.alpha = 0
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                    self.setAvarageColor(for: track)
                })
            }
        }
    }
    
    fileprivate func setAvarageColor(for track: Track) {
        let iv = CachedImageView()
        iv.loadImage(urlString: track.album.imageUrl, completion: {
            _ = iv.image?.getAvarageColor(completion: { (color) in
                DispatchQueue.main.async {
                    UIView.animate(withDuration: 0.5, animations: {
                        self.playerContainerOverlayColor.backgroundColor = color.withAlphaComponent(0.9)
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
                            UIView.animate(withDuration: 4, animations: {
                                self.playerContainerImageView.image = iv.image
                                self.playerContainerImageView.alpha = 1
                            })
                        })
                    })
                }
            })
        })
    }
    
    @objc internal func handleSliderChange() {
        if let durationTime = self.player?.currentItem?.asset.duration {
            customTabBarController?.config.panRecognier.isEnabled = false
            let seconds = CMTimeGetSeconds(durationTime)
            let value = Float64(self.duration.slider.value) * seconds
            let seekTime = CMTime(value: Int64(value), timescale: 1)
            player?.seek(to: seekTime, completionHandler: { (completed) in
                self.customTabBarController?.config.panRecognier.isEnabled = true
            })
        }
    }
    
    internal func setupDuration() {
        let interval = CMTime(seconds: 1.0, preferredTimescale: 1)
        let mainQueue = DispatchQueue.main
        
        DispatchQueue.main.async {
            if let durationTime = self.player?.currentItem?.asset.duration {
                let seconds = CMTimeGetSeconds(durationTime)
                let secondsText = String(format: "%02d", Int(seconds)%60)
                let minutesText = String(format: "%02d", Int(seconds)/60)
                self.duration.endDuration.text = "\(minutesText):\(secondsText)"
                
                self.player?.addPeriodicTimeObserver(forInterval: interval, queue: mainQueue) { time in
                    if let status = self.player?.timeControlStatus {
                        if status == .playing {
                            let seconds = CMTimeGetSeconds(time)
                            let secondsText = String(format: "%02d", Int(seconds)%60)
                            let minutesText = String(format: "%02d", Int(seconds)/60)
                            self.duration.startDuration.text = "\(minutesText):\(secondsText)"
                            
                            let durationSeconds = CMTimeGetSeconds(durationTime)
                            self.duration.slider.value = Float(seconds/durationSeconds)
                            self.progressBarFixed.value = Float(seconds/durationSeconds)
                        }
                    }
                }
            }
            
        }
    }    
    
}
