//
//  PlaytTrack.swift
//  Spotify-Demo
//
//  Created by Andrea Toso on 13/10/2018.
//  Copyright © 2018 Andrea Toso. All rights reserved.
//

import Foundation
import AVFoundation
import UIKit

protocol TrackControllerDelegate: class {
    func playTrack(track: Track)
}

extension TrackController: TrackControllerDelegate {

    func playTrack(track: Track) {
        DispatchQueue.main.async {
            if !self.tracks.isEmpty {
                self.player?.pause()
                self.setupInfo(track: track)
                self.setupUI(for: track)
                self.startPlayer(with: track)
                self.currentTrack = track
            }
        }
    }
    
    fileprivate func startPlayer(with track: Track) {
        var artist = ""
        
        track.artists.forEach({artist += "\($0.name), "})
        Service.sharedInstance.youtubeService.search(query: "\(artist) \(track.name)") { (url) in
            if let urlString = URL(string: url) {
                
                let asset = AVAsset(url: urlString)
                let keys: [String] = ["playable"]
                
                asset.loadValuesAsynchronously(forKeys: keys, completionHandler: {
                    var error: NSError? = nil
                    let status = asset.statusOfValue(forKey: "playable", error: &error)
                    switch status {
                    case .loaded:
                        DispatchQueue.main.async {
                            let item = AVPlayerItem(asset: asset)
                            self.player = AVPlayer(playerItem: item)
                            self.player?.play()
                            
                            self.controls.playButton.setImage(UIImage(named: "pause_button")?.withRenderingMode(.alwaysTemplate), for: .normal)
                            self.playButtonFixed.setImage(UIImage(named: "pause_circle")?.withRenderingMode(.alwaysTemplate), for: .normal)
                            
                            self.setupDuration()
                            self.duration.slider.addTarget(self, action: #selector(self.handleSliderChange), for: .valueChanged)
                        }
                    default:
                        break
                    }
                })
                
            }
        }
    }
    
}
