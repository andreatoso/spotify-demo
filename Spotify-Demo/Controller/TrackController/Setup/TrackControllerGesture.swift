//
//  TrackControllerGesture.swift
//  Spotify-Demo
//
//  Created by Andrea Toso on 13/09/18.
//  Copyright © 2018 Andrea Toso. All rights reserved.
//

import UIKit

extension CustomTabBarController {
    
    @objc internal func panned(recognizer: UIPanGestureRecognizer) {
        displayLink.isPaused = false
        switch recognizer.state {
        case .began:
            beganState()
        case .changed:
            changedState(recognizer: recognizer)
        case .ended, .cancelled:
            endedState(recognizer: recognizer)
        default: break
        }
    }
    
    internal func beganState() {
        startAnimationIfNeeded()
        config.animator.pauseAnimation()
        config.animationProgress = config.animator.fractionComplete
    }
    
    fileprivate func changedState(recognizer: UIPanGestureRecognizer) {
        var fraction = -recognizer.translation(in: trackController).y / config.closedTransform.ty
        if config.isOpen { fraction *= -1 }
        if config.animator.isReversed { fraction *= -1 }
        config.animator.fractionComplete = fraction + config.animationProgress
    }
    
    internal func endedState(recognizer: UIPanGestureRecognizer) {
        if !config.isMoving {
            self.trackController.isUserInteractionEnabled = false
            self.config.isMoving = true
        }
        
        let yVelocity = recognizer.velocity(in: trackController).y
        let shouldClose = yVelocity > 0
        if yVelocity == 0 {
            if !config.isOpen {
                config.animator.continueAnimation(withTimingParameters: nil, durationFactor: 0)
                return
            }
        }
        if config.isOpen {
            if !shouldClose && !config.animator.isReversed { config.animator.isReversed = !config.animator.isReversed }
            if shouldClose && config.animator.isReversed { config.animator.isReversed = !config.animator.isReversed }
        } else {
            if shouldClose && !config.animator.isReversed { config.animator.isReversed = !config.animator.isReversed }
            if !shouldClose && config.animator.isReversed { config.animator.isReversed = !config.animator.isReversed }
        }
        let fractionRemaining = 1 - config.animator.fractionComplete
        let distanceRemaining = fractionRemaining * config.closedTransform.ty
        if distanceRemaining == 0 {
            config.animator.continueAnimation(withTimingParameters: nil, durationFactor: 0)
            return
        }
        let timingParameters = UISpringTimingParameters()
        let preferredDuration = UIViewPropertyAnimator(duration: 0, timingParameters: timingParameters).duration
        let durationFactor = CGFloat(preferredDuration / config.animator.duration)
        config.animator.continueAnimation(withTimingParameters: timingParameters, durationFactor: durationFactor)
    }
    
    internal func startAnimationIfNeeded() {
        if config.animator.isRunning { return }
        let timingParameters = UISpringTimingParameters()
        config.animator = UIViewPropertyAnimator(duration: 0, timingParameters: timingParameters)
        config.animator.addAnimations {
            self.trackController.transform = self.config.isOpen ? self.config.closedTransform : .identity
            
            if !self.config.isOpen {
                self.tabBar.frame = (self.tabBar.frame.offsetBy(dx: 0, dy: self.tabBar.frame.height))
            } else {
                self.tabBar.frame = (self.tabBar.frame.offsetBy(dx: 0, dy: -self.tabBar.frame.height))
            }
        }
        config.animator.addCompletion { position in
            if position == .end { self.config.isOpen = !self.config.isOpen }
            self.config.isMoving = false
            self.trackController.isUserInteractionEnabled = true
            self.displayLink.isPaused = true
//            _ = UIApplication.shared.setStatusBarHidden(self.config.isOpen, with: .slide)
        }
        config.animator.startAnimation()
    }
    
    @objc internal func handleTrackControllerTransition(displayLink: CADisplayLink) {
        let presentationLayer = trackController.layer.presentation()
        guard let offset = presentationLayer?.frame.minY else {return}
        guard let tabBarY = tabBar.layer.presentation()?.frame.minY else {return}
        guard let containerHeight = trackController.fixedBottomContainer.layer.presentation()?.frame.height else {return}
        
        let x1: CGFloat = -(containerHeight - tabBarY)
        let x2: CGFloat = x1 - 32
        self.trackController.fixedBottomContainer.alpha = getAlpha(x: offset, x1: x1, x2: x2, y1: 1, y2: 0)
        self.trackController.playerContainer.alpha = getAlpha(x: offset, x1: x1, x2: x2, y1: 0, y2: 1)
    }
    
    fileprivate func getAlpha(x: CGFloat, x1: CGFloat, x2: CGFloat, y1: CGFloat, y2: CGFloat) -> CGFloat {
        let m: CGFloat = (y2-y1)/(x2-x1)
        let q: CGFloat = y1-m*(x1)
        return m*(x)+q
    }
    
}


