//
//  TrackController.swift
//  Spotify-Demo
//
//  Created by Andrea Toso on 12/09/18.
//  Copyright © 2018 Andrea Toso. All rights reserved.
//

import UIKit
import UIKit.UIGestureRecognizerSubclass
import AVFoundation

class TrackController: UIView, UIGestureRecognizerDelegate {
    
    var player: AVPlayer?
    
    var customTabBarController: CustomTabBarController?
    var indexPath: IndexPath = IndexPath() {didSet{trackCV.indexPath = indexPath}}
    var tracks: [Track] = [] {didSet{trackCV.tracks = tracks}}
    var currentTrack: Track?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = Color.Black
        isHidden = true
        
        setupFixedBottomContainer()
        setupPlayer()
        setupAVPlayer()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func setupAVPlayer() {
        do {
            try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default, options: [.mixWithOthers])
            print("AVAudioSession Category Playback OK")
            try AVAudioSession.sharedInstance().setActive(true)
            print("AVAudioSession is Active")
            NotificationCenter.default.addObserver(self, selector: #selector(playerDidFinishPlaying(note:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: player?.currentItem)
        } catch let error as NSError {
            print(error.localizedDescription)
        }
    }
    
    let momentumView: UIView = {
        let view = UIView()
        view.backgroundColor = .black
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let fixedBottomContainer: UIView = {
        let view = UIView()
        view.backgroundColor = Color.Gray
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let progressBarFixed: UISlider = {
        let slider = UISlider()
        slider.minimumTrackTintColor = UIColor.white
        slider.maximumTrackTintColor = UIColor.white.withAlphaComponent(0.1)
        slider.setThumbImage(UIImage(), for: .normal)
        slider.isUserInteractionEnabled = false
        return slider
    }()
    
    let likeButtonFixed: UIButton = {
        let button = UIButton(type: .custom)
        button.setImage(UIImage(named: "like")?.withRenderingMode(.alwaysTemplate), for: .normal)
        button.imageView?.tintColor = UIColor.white
        button.imageView?.contentMode = .scaleAspectFit
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    let trackNameFixed: UILabel = {
        let label = UILabel()
        label.font = UIFont.init(name: Font.Book, size: 12.25)
        label.textAlignment = .center
        
        let trackString = NSMutableAttributedString(string: "Better Now • ", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        let artistString = NSAttributedString(string: "Post Malone", attributes: [NSAttributedString.Key.foregroundColor: Color.LightGray])
        trackString.append(artistString)
        label.attributedText = trackString
        
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let playButtonFixed: UIButton = {
        let button = UIButton(type: .custom)
        button.setImage(UIImage(named: "play_circle")?.withRenderingMode(.alwaysTemplate), for: .normal)
        button.imageView?.tintColor = UIColor.white
        button.imageView?.contentMode = .scaleAspectFit
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    let availableDevices: UILabel = {
        let label = UILabel()
        label.text = "Dispositivi disponibili"
        label.textColor = UIColor.white
        label.font = UIFont.init(name: Font.Book, size: 10.5)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let devicesButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setImage(UIImage(named: "devices"), for: .normal)
        button.imageView?.contentMode = .scaleAspectFit
        button.isUserInteractionEnabled = false
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    func setupFixedBottomContainer() {
        let fixedBottomContainerHeight: CGFloat = TrackControllerConfig().fixedBottomContainerHeight
        addSubview(fixedBottomContainer)
        fixedBottomContainer.anchor(topAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: fixedBottomContainerHeight)
        
        fixedBottomContainer.addSubview(progressBarFixed)
        progressBarFixed.anchor(fixedBottomContainer.topAnchor, left: fixedBottomContainer.leftAnchor, bottom: nil, right: fixedBottomContainer.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0.5)
        
        fixedBottomContainer.addSubview(likeButtonFixed)
        likeButtonFixed.centerYAnchor.constraint(equalTo: fixedBottomContainer.centerYAnchor, constant: -2).isActive = true
        likeButtonFixed.leftAnchor.constraint(equalTo: fixedBottomContainer.leftAnchor, constant: 15).isActive = true
        likeButtonFixed.heightAnchor.constraint(equalToConstant: 21).isActive = true
        likeButtonFixed.widthAnchor.constraint(equalToConstant: 21).isActive = true
        
        fixedBottomContainer.addSubview(playButtonFixed)
        playButtonFixed.centerYAnchor.constraint(equalTo: fixedBottomContainer.centerYAnchor, constant: -2).isActive = true
        playButtonFixed.rightAnchor.constraint(equalTo: fixedBottomContainer.rightAnchor, constant: -15).isActive = true
        playButtonFixed.heightAnchor.constraint(equalToConstant: 26).isActive = true
        playButtonFixed.widthAnchor.constraint(equalToConstant: 26).isActive = true
        playButtonFixed.addTarget(self, action: #selector(controlTrack), for: .touchUpInside)
        
        fixedBottomContainer.addSubview(trackNameFixed)
        trackNameFixed.leftAnchor.constraint(equalTo: likeButtonFixed.rightAnchor, constant: 15).isActive = true
        trackNameFixed.rightAnchor.constraint(equalTo: playButtonFixed.leftAnchor, constant: -15).isActive = true
        trackNameFixed.centerYAnchor.constraint(equalTo: fixedBottomContainer.centerYAnchor, constant: -8.5).isActive = true
        
        fixedBottomContainer.addSubview(availableDevices)
        availableDevices.centerYAnchor.constraint(equalTo: fixedBottomContainer.centerYAnchor, constant: 8).isActive = true
        availableDevices.centerXAnchor.constraint(equalTo: fixedBottomContainer.centerXAnchor, constant: 7).isActive = true
        
        fixedBottomContainer.addSubview(devicesButton)
        devicesButton.centerYAnchor.constraint(equalTo: availableDevices.centerYAnchor, constant: 1).isActive = true
        devicesButton.rightAnchor.constraint(equalTo: availableDevices.leftAnchor, constant: -5).isActive = true
        devicesButton.heightAnchor.constraint(equalToConstant: 11).isActive = true
        devicesButton.widthAnchor.constraint(equalToConstant: 12).isActive = true
    }
    
    let playerContainer: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let playerContainerImageView: UIImageView = {
        let view = UIImageView()
        view.contentMode = .scaleAspectFill
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let playerContainerOverlayColor: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let playerContainerOverlay: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .equalSpacing
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    let header: TrackHeader = {
        let view = TrackHeader()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let trackCV: TrackCV = {
        let view = TrackCV()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let name: TrackName = {
        let view = TrackName()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let duration: TrackDuration = {
        let view = TrackDuration()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let controls: TrackControls = {
        let view = TrackControls()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let footer: TrackFooter = {
        let view = TrackFooter()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    func setupPlayer() {
        let statusBarHeight = UIApplication.shared.statusBarFrame.height
        guard let safeAreaBottom = UIApplication.shared.keyWindow?.safeAreaInsets.bottom else {return}
        
        addSubview(playerContainer)
        playerContainer.fillSuperview()
        bringSubviewToFront(fixedBottomContainer)
        
        playerContainer.addSubview(playerContainerImageView)
        playerContainerImageView.fillSuperview()
        
        playerContainer.addSubview(playerContainerOverlayColor)
        playerContainerOverlayColor.fillSuperview()
        
        playerContainer.addSubview(playerContainerOverlay)
        playerContainerOverlay.fillSuperview()
        
        playerContainer.addSubview(stackView)
        stackView.anchor(playerContainer.topAnchor, left: playerContainer.leftAnchor, bottom: playerContainer.bottomAnchor, right: playerContainer.rightAnchor, topConstant: statusBarHeight + 22, leftConstant: 0, bottomConstant: safeAreaBottom + 24, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        stackView.addArrangedSubview(header)
        stackView.addArrangedSubview(trackCV)
        trackCV.trackController = self
        trackCV.tracks = self.tracks
        trackCV.delegate = self
        stackView.addArrangedSubview(name)
        stackView.addArrangedSubview(duration)
        stackView.addArrangedSubview(controls)
        controls.playButton.addTarget(self, action: #selector(controlTrack), for: .touchUpInside)
        controls.nextButton.addTarget(self, action: #selector(playNextTrack), for: .touchUpInside)
        controls.previousButton.addTarget(self, action: #selector(playPreviousTrack), for: .touchUpInside)
        stackView.addArrangedSubview(footer)
        
        let gesture = UILongPressGestureRecognizer(target: self, action: #selector(handleTrackControllerRecognizer))
        gesture.minimumPressDuration = 0
        gesture.delegate = self
        trackCV.addGestureRecognizer(gesture)
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    @objc func handleTrackControllerRecognizer(recognizer: UIGestureRecognizer) {
        switch recognizer.state {
        case .began:
            customTabBarController?.config.panRecognier.isEnabled = false
        case .ended, .cancelled:
            customTabBarController?.config.panRecognier.isEnabled = true
        default:
            break
        }
    }
    
    fileprivate func findHeight(view: UIView, sum: Bool) -> CGFloat {
        var height: [CGFloat] = []
        view.subviews.forEach { (subview) in
            height.append(subview.frame.height)
        }
        if sum {
            return height.reduce(0, +)
        } else {
            guard let finalHeight = height.max() else {return 0}
            return finalHeight
        }
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        layoutIfNeeded()
        
        header.heightAnchor.constraint(equalToConstant: findHeight(view: header, sum: false) + header.source.frame.height).isActive = true
        trackCV.heightAnchor.constraint(equalToConstant: UIScreen.main.bounds.height*0.43).isActive = true
        name.heightAnchor.constraint(equalToConstant: findHeight(view: name, sum: true)).isActive = true
        duration.heightAnchor.constraint(equalToConstant: findHeight(view: duration, sum: false)).isActive = true
        controls.heightAnchor.constraint(equalToConstant: findHeight(view: controls, sum: false)).isActive = true
        footer.heightAnchor.constraint(equalToConstant: findHeight(view: footer, sum: false)).isActive = true
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        duration.customTabBarController = self.customTabBarController
    }
    
}
