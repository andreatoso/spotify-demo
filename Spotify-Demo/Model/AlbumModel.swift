//
//  AlbumModel.swift
//  Spotify-Demo
//
//  Created by Andrea Toso on 21/09/18.
//  Copyright © 2018 Andrea Toso. All rights reserved.
//

import Foundation
import SwiftyJSON

struct Album: Mappable {
    let artists: [ArtistProfile]
    let id: String
    let imageUrl: String
    let name: String
    let popularity: Int
    let releaseDate: String
    let numberOfTracks: Int
    let tracks: [Track]
    
    init(json: JSON) {
        let artistsObject = json["artists"].arrayValue
        var artistsArray: [ArtistProfile] = []
        for artist in artistsObject {
            artistsArray.append(ArtistProfile(json: artist))
        }
        
        self.artists = artistsArray
        self.id = json["id"].stringValue
        let images = json["images"].arrayValue
        if !images.isEmpty {
            let image = images[0]
            self.imageUrl = image["url"].stringValue
        } else {
            self.imageUrl = ""
        }
        self.name = json["name"].stringValue
        self.popularity = json["popularity"].intValue
        self.releaseDate = json["release_date"].stringValue
        self.numberOfTracks = json["total_tracks"].intValue
        let tracksObject = json["tracks"]
        let tracksItems = tracksObject["items"].arrayValue
        var tracksArray: [Track] = []
        tracksItems.forEach({tracksArray.append(Track(json: $0))})
        self.tracks = tracksArray
    }
}
