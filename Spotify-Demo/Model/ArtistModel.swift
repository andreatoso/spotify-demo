//
//  ArtistModel.swift
//  Spotify-Demo
//
//  Created by Andrea Toso on 05/09/18.
//  Copyright © 2018 Andrea Toso. All rights reserved.
//

import Foundation
import SwiftyJSON

struct Artist: Mappable {
    let id: String
    let name: String
    let profileImageUrl: String
    let followers: Int
    let popularity: Int
    
    init(json: JSON) {
        let followers = json["followers"]
        let followersNumber = followers["total"].intValue
        let id = json["id"].stringValue
        let images = json["images"].arrayValue
        var url = ""
        if !images.isEmpty {
            let image = images[0]
            url = image["url"].stringValue
        }
        let name = json["name"].stringValue
        let popularity = json["popularity"].intValue
        
        self.id = id
        self.name = name
        self.profileImageUrl = url
        self.followers = followersNumber
        self.popularity = popularity
    }
}

struct ArtistProfile: Mappable {
    let id: String
    let name: String
    
    init(json: JSON) {
        self.id = json["id"].stringValue
        self.name = json["name"].stringValue
    }
}

struct ArtistDetails {
    var latestRelease: Album?
    let playlist: Playlist?
    let topTracks: [Track]?
    let similarArtists: [Artist]?
    let albums: [Album]?
}

struct ArtistTourList {
    let tour: [ArtistTour]
}

struct ArtistTour {
    let date: String
    let artists: [String]
    let address: String
}
