//
//  LibraryModel.swift
//  Spotify-Demo
//
//  Created by Andrea Toso on 11/09/18.
//  Copyright © 2018 Andrea Toso. All rights reserved.
//

import Foundation

//struct LibraryTrack {
//    let trackName: String
//    let isTrackExplicit: Bool
//    let trackArtists: [String]
//}

struct Library {
    let id: String
    let type: LibraryType
}

enum LibraryType {
    case artist
    case album
    case playlist
}
