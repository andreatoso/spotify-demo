//
//  MappableModelProtocol.swift
//  Spotify-Demo
//
//  Created by Andrea Toso on 23/09/18.
//  Copyright © 2018 Andrea Toso. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol Mappable {
    init(json: JSON)
}
