//
//  PlaylistModel.swift
//  Spotify-Demo
//
//  Created by Andrea Toso on 20/09/18.
//  Copyright © 2018 Andrea Toso. All rights reserved.
//

import Foundation
import SwiftyJSON

struct Playlist: Mappable {
    let description: String
    let followers: Int
    let id: String
    let imageUrl: String
    let name: String
    let owner: PlaylistOwner
    let tracks: [Track]
    
    init(json: JSON) {
        self.description = json["description"].stringValue
        let followersObject = json["followers"]
        self.followers = followersObject["total"].intValue
        self.id = json["id"].stringValue
        let images = json["images"].arrayValue
        if !images.isEmpty {
            let image = images[0]
            self.imageUrl = image["url"].stringValue
        } else {
            self.imageUrl = ""
        }
        self.name = json["name"].stringValue
        let owner = json["owner"]
        self.owner = PlaylistOwner(name: owner["display_name"].stringValue, id: owner["id"].stringValue)
        let tracksObject = json["tracks"]
        let items = tracksObject["items"].arrayValue
        var tracksArray: [Track] = []
        for item in items {
            let track = item["track"]
            tracksArray.append(Track(json: track))
        }
        self.tracks = tracksArray
    }
}

struct FeaturedPlaylist {
    let id: String
    let imageUrl: String
    let name: String
    
    init(json: JSON) {
        self.id = json["id"].stringValue
        let images = json["images"].arrayValue
        if !images.isEmpty {
            let image = images[0]
            self.imageUrl = image["url"].stringValue
        } else {
            self.imageUrl = ""
        }
        self.name = json["name"].stringValue
    }
}

struct PlaylistOwner {
    let name: String
    let id: String
}
