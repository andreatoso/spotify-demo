//
//  SearchModel.swift
//  Spotify-Demo
//
//  Created by Andrea Toso on 25/08/18.
//  Copyright © 2018 Andrea Toso. All rights reserved.
//

import Foundation

struct SearchResult {
    var relevantResult: SearchRelevantResult?
    let artists: [Artist]
    let tracks: [Track]
    let albums: [Album]
    let playlists: [Playlist]
}

struct SearchRelevantResult {
    let id: String
    let type: RelevantResult
    let popularity: Int
}

enum RelevantResult {
    case tracks([Track])
    case artists([Artist])
    case track
    case artist
}
