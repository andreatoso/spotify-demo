//
//  HorizontalModel.swift
//  Spotify-Demo
//
//  Created by Andrea Toso on 20/09/18.
//  Copyright © 2018 Andrea Toso. All rights reserved.
//

import Foundation
import SwiftyJSON

struct Track: Mappable {
    var album: Album
    let artists: [ArtistProfile]
    let duration: Int
    let isExplicit: Bool
    let id: String
    let name: String
    let popularity: Int
    
    init(json: JSON) {
        self.album = Album(json: json["album"])
        let artistsObject = json["artists"].arrayValue
        var artistsArray: [ArtistProfile] = []
        for artist in artistsObject {
            artistsArray.append(ArtistProfile(json: artist))
        }
        self.artists = artistsArray
        self.duration = json["duration_ms"].intValue
        self.isExplicit = json["explicit"].boolValue
        self.id = json["id"].stringValue
        self.name = json["name"].stringValue
        self.popularity = json["popularity"].intValue
    }
}
