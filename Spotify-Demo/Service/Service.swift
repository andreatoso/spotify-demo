//
//  Service.swift
//  Spotify-Demo
//
//  Created by Andrea Toso on 19/09/18.
//  Copyright © 2018 Andrea Toso. All rights reserved.
//

import Foundation

struct Service {
    
    static let sharedInstance = Service()
    let spotifyService = SpotifyService()
    let youtubeService = YoutubeService()
    
}
