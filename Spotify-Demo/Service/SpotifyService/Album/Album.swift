//
//  Album.swift
//  Spotify-Demo
//
//  Created by Andrea Toso on 21/09/18.
//  Copyright © 2018 Andrea Toso. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

extension SpotifyService {
    
    /**
     Get Spotify catalog information for a single album.
     
     - parameter id: The Spotify ID for the album.
     - parameter completion: Execute task to return requested album.
     */
    
    public func getAlbum(id: String, completion: @escaping (Album) -> ()) {
        Service.sharedInstance.spotifyService.getToken { (token) in
            let headers: HTTPHeaders = ["Authorization": "Bearer \(token)"]
            Alamofire.request("https://api.spotify.com/v1/albums/\(id)", headers: headers).responseJSON { response in
                if let data = response.data {
                    do {
                        let json = try JSON(data: data)
                        completion(Album(json: json))
                    } catch {
                        print("Failed to pass recently played json response object: \(error)")
                    }
                } else {
                    print("Failed to fetch response")
                }
            }
        }
    }
    
}

extension SpotifyService {
    
    /**
     Get Spotify catalog information about an artist’s albums.
     
     - parameter artistId: The Spotify ID for the artist.
     - parameter completion: Execute task to return an array containing artist's albums.
     */
    
    public func getArtistAlbums(artistId: String, completion: @escaping ([Album]) -> ()) {
        Service.sharedInstance.spotifyService.getToken { (token) in
            let headers: HTTPHeaders = ["Authorization": "Bearer \(token)"]
            Alamofire.request("https://api.spotify.com/v1/artists/\(artistId)/albums?include_groups=album", headers: headers).responseJSON { response in
                if let data = response.data {
                    do {
                        let json = try JSON(data: data)
                        completion(self.handleResponse(json: json))
                    } catch {
                        print("Failed to pass recently played json response object: \(error)")
                    }
                } else {
                    print("Failed to fetch response")
                }
            }
        }
    }
    
    fileprivate func handleResponse(json: JSON) -> [Album] {
        var albums: [Album] = []
        let items = json["items"].arrayValue
        for item in items {
            albums.append(Album(json: item))
        }
        return albums
    }
    
}
