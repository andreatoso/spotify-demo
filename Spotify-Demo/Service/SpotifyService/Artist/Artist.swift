//
//  Artist.swift
//  Spotify-Demo
//
//  Created by Andrea Toso on 21/09/18.
//  Copyright © 2018 Andrea Toso. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

extension SpotifyService {
    
    /**
     Get Spotify catalog information for a single artist identified by their unique Spotify ID.
     
     - parameter id: The Spotify ID for the artist.
     - parameter completion: Execute task to return requested Artist Profile.
     */
    
    public func getArtistProfile(id: String, completion: @escaping (Artist) -> ()) {
        Service.sharedInstance.spotifyService.getToken { (token) in
            let headers: HTTPHeaders = ["Authorization": "Bearer \(token)"]
            Alamofire.request("https://api.spotify.com/v1/artists/\(id)", headers: headers).responseJSON { response in
                if let data = response.data {
                    do {
                        let json = try JSON(data: data)
                        completion(Artist(json: json))
                    } catch {
                        print("Failed to pass recently played json response object: \(error)")
                    }
                } else {
                    print("Failed to fetch response")
                }
            }
        }
    }
    
}
