//
//  ArtistDetails.swift
//  Spotify-Demo
//
//  Created by Andrea Toso on 29/09/2018.
//  Copyright © 2018 Andrea Toso. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

extension SpotifyService {
    
    /**
     Get Spotify catalog information for a single artist identified by their unique Spotify ID.
     
     - parameter id: The Spotify ID for the artist.
     - parameter completion: Execute task to return requested Artist details.
     */
    
    public func getArtistDetails(artist: Artist, completion: @escaping (ArtistDetails) -> ()) {
        let group = DispatchGroup()
        var latestRelease: Album? {didSet {group.leave()}}
        var artistPlaylist: Playlist? {didSet {group.leave()}}
        var artistTopTracks: [Track]? {didSet {group.leave()}}
        var similarArtists: [Artist]? {didSet {group.leave()}}
        var artistAlbums: [Album]? {didSet {group.leave()}}
        
        group.enter()
        getArtistLatestRelease(id: artist.id) { (album) in latestRelease = album }
        
        group.enter()
        getArtistFeaturedPlaylist(name: artist.name) { (playlist) in artistPlaylist = playlist }
        
        group.enter()
        getArtistTopTracks(id: artist.id) { (tracks) in artistTopTracks = tracks }
        
        group.enter()
        getArtistSimilar(id: artist.id) { (artists) in similarArtists = artists }
        
        group.enter()
        getArtistAlbums(artistId: artist.id) { (albums) in artistAlbums = albums }
        
        group.notify(queue: .main) {
            completion(ArtistDetails(latestRelease: latestRelease, playlist: artistPlaylist, topTracks: artistTopTracks, similarArtists: similarArtists, albums: artistAlbums))
        }
    }
    
}
