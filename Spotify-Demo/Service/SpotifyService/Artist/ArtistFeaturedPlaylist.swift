//
//  Playlist.swift
//  Spotify-Demo
//
//  Created by Andrea Toso on 28/09/18.
//  Copyright © 2018 Andrea Toso. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

extension SpotifyService {
    
    /**
     Get Spotify Artist's featured playlist.
     
     - parameter name: The name of artist.
     - parameter completion: Execute task to return requested Artist's featured playlist.
     */
    
    public func getArtistFeaturedPlaylist(name: String, completion: @escaping (Playlist) -> ()) {
        Service.sharedInstance.spotifyService.getToken { (token) in
            guard let query = name.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) else {return}
            let headers: HTTPHeaders = ["Authorization": "Bearer \(token)"]
            
            Alamofire.request("https://api.spotify.com/v1/search?q=\(query)&type=playlist&limit=5", headers: headers).responseJSON { response in
                if let data = response.data {
                    do {
                        let json = try JSON(data: data)
                        self.handleResponse(json: json, completion: { (playlist) in
                            completion(playlist)
                        })
                    } catch {
                        print("Failed to pass recently played json response object: \(error)")
                    }
                } else {
                    print("Failed to fetch response")
                }
            }
        }
    }
    
    fileprivate func handleResponse(json: JSON, completion: @escaping (Playlist) -> ()) {
        let playlistsItem = json["playlists"]
        let playlists = playlistsItem["items"].arrayValue.map({Playlist(json: $0)})
        guard let playlist = playlists.first(where: {$0.owner.id != "spotify"}) else {
            print("Failed to handle getArtistsPlaylist response")
            return
        }
        var playlistsArray: [Playlist] = []
        let group = DispatchGroup()
        Service.sharedInstance.spotifyService.getPlaylists(for: playlist.owner.id) { (playlistsResult) in
            playlistsResult.forEach({ (playlistObject) in
                group.enter()
                Service.sharedInstance.spotifyService.getPlaylist(id: playlistObject.id, completion: { (play) in
                    playlistsArray.append(play)
                    group.leave()
                })
            })
            group.notify(queue: .main, execute: {
                playlistsArray.sort(by: {$0.followers > $1.followers})
                completion(playlistsArray[0])
            })
        }
    }
    
}
