//
//  LatestRelease.swift
//  Spotify-Demo
//
//  Created by Andrea Toso on 29/09/2018.
//  Copyright © 2018 Andrea Toso. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

extension SpotifyService {
    
    /**
     Get Artist's album latest release.
     
     - parameter id: The Spotify ID for the artist.
     - parameter completion: Execute task to return requested Artists's latest release.
     */
    
    public func getArtistLatestRelease(id: String, completion: @escaping (Album?) -> ()) {
        Service.sharedInstance.spotifyService.getArtistAlbums(artistId: id) { (albums) in
            var albumsArray = albums
            albumsArray.sort(by: {$0.releaseDate > $1.releaseDate})
            guard let release = albumsArray.first else {
                completion(nil)
                return
            }
            completion(release)
        }
    }
    
}
