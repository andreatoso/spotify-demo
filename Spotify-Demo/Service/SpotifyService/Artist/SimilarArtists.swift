//
//  SimilarArtists.swift
//  Spotify-Demo
//
//  Created by Andrea Toso on 28/09/18.
//  Copyright © 2018 Andrea Toso. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

extension SpotifyService {
    
    /**
     Get Spotify catalog information about an artist’s top tracks by country.
     
     - parameter id: The Spotify ID for the artist.
     - parameter completion: Execute task to return requested Artist's top tracks.
     */
    
    public func getArtistSimilar(id: String, completion: @escaping ([Artist]) -> ()) {
        Service.sharedInstance.spotifyService.getToken { (token) in
            let headers: HTTPHeaders = ["Authorization": "Bearer \(token)"]
            Alamofire.request("https://api.spotify.com/v1/artists/\(id)/related-artists", headers: headers).responseJSON { response in
                if let data = response.data {
                    do {
                        let json = try JSON(data: data)
                        let tracks = json["artists"].arrayValue.map({Artist(json: $0)})
                        completion(tracks)
                    } catch {
                        print("Failed to pass recently played json response object: \(error)")
                    }
                } else {
                    print("Failed to fetch response")
                }
            }
        }
    }
    
}
