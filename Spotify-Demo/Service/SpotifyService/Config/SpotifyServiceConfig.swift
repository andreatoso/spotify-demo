//
//  ServiceConfig.swift
//  Spotify-Demo
//
//  Created by Andrea Toso on 19/09/18.
//  Copyright © 2018 Andrea Toso. All rights reserved.
//

import Foundation

struct SpotifyServiceConfig {
    
    private let clientId = "494658e7af4f476d9f85c89b8b710209"
    private let clientSecret = "b5e98ebe972c4e97b439ccb164e5fc4b"
    let spotifyTokenKey = "spotifyTokenKey"
    var basicToken: String
    let refreshToken = "AQAYlNGaiyoJNwXWl7ztWHAa8ioZzgaaj1_z-6nctnRhk5Hmer2R1B3wbyY9htfVeGsLkJK4svZtkANbPgrl3w7ejNrPzm8gUieXxc8L5XdWQGARriRCD1yOmuzf2y4ALjRcCg"
    
    init() {
        let encoded = "\(clientId):\(clientSecret)".data(using: .utf8)
        if let token = encoded?.base64EncodedString() {
            basicToken = token
        } else {
            basicToken = "Failed to generate basicToken"
        }
    }
    
}

struct SpotifyToken: Codable {
    let token: String
    let date: Date
    
    var dictionary: [String: Any] {
        return ["token" : token, "date" : date]
    }
}
