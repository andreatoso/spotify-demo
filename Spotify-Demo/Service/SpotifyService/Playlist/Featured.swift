//
//  Featured.swift
//  Spotify-Demo
//
//  Created by Andrea Toso on 20/09/18.
//  Copyright © 2018 Andrea Toso. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

extension SpotifyService {
    
    /**
     Get a list of Spotify featured playlists.
     
     - parameter completion: Execute task to return an array containing Spotify featured playlists.
     */
    
    public func getFeaturedPlaylists(completion: @escaping ([FeaturedPlaylist]) -> ()) {
        Service.sharedInstance.spotifyService.getToken { (token) in
            let headers: HTTPHeaders = ["Authorization": "Bearer \(token)",]
            Alamofire.request("https://api.spotify.com/v1/browse/featured-playlists?country=US&limit=10", headers: headers).responseJSON { response in
                if let data = response.data {
                    do {
                        let json = try JSON(data: data)
                        completion(self.handleResponse(json: json))
                    } catch {
                        print("Failed to pass recently played json response object: \(error)")
                    }
                } else {
                    print("Failed to fetch response")
                }
            }
        }
    }
    
    fileprivate func handleResponse(json: JSON) -> [FeaturedPlaylist] {
        let playlists = json["playlists"]
        let items = playlists["items"].arrayValue
        
        var featuredPlaylists: [FeaturedPlaylist] = []
        for item in items {
            featuredPlaylists.append(FeaturedPlaylist(json: item))
        }
        return featuredPlaylists
    }
    
}
