//
//  File.swift
//  Spotify-Demo
//
//  Created by Andrea Toso on 20/09/18.
//  Copyright © 2018 Andrea Toso. All rights reserved.
//z

import Foundation

extension SpotifyService {
    
    /**
     Get Spotify Made For You playlists.
     
     - parameter completion: Execute task an array containing Spotify Daily Mix playlists.
     */
    
    public func getDailyMix(completion: @escaping ([Playlist]) -> ()) {
        var madeForYouPlaylists: [Int: Playlist] = [:]
        
        let discoverWeekly = "37i9dQZEVXcTjMc2IKbto9"
        let releaseRadar = "37i9dQZEVXbpcxhUSiO7Sr"
        let topGlobal = "37i9dQZEVXbMDoHDwVN2tF"
        let ids = [1: discoverWeekly, 2: releaseRadar, 3: topGlobal]
        
        let group = DispatchGroup()
        
        for (key, value) in ids {
            group.enter()
            getPlaylist(id: value) { (playlist) in
                madeForYouPlaylists[key] = playlist
                group.leave()
            }
        }
        
        group.notify(queue: .main) {
            let sorted = Array(madeForYouPlaylists).sorted(by: {$0.0 < $1.0})
            var array: [Playlist] = []
            for (_, value) in sorted {
                array.append(value)
            }
            completion(array)
        }
    }
    
}
