//
//  Playlist.swift
//  Spotify-Demo
//
//  Created by Andrea Toso on 20/09/18.
//  Copyright © 2018 Andrea Toso. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

extension SpotifyService {
    
    /**
     Get a playlist owned by a Spotify user.
     
     - parameter id: The Spotify ID for the playlist.
     - parameter completion: Execute task to return requested playlist.
     */
    
    public func getPlaylist(id: String, completion: @escaping (Playlist) -> ()) {
        Service.sharedInstance.spotifyService.getToken { (token) in
            let headers: HTTPHeaders = ["Authorization": "Bearer \(token)",]
            Alamofire.request("https://api.spotify.com/v1/playlists/\(id)", headers: headers).responseJSON { response in
                if let data = response.data {
                    do {
                        let json = try JSON(data: data)
                        completion(Playlist(json: json))
                    } catch {
                        print("Failed to pass recently played json response object: \(error)")
                    }
                } else {
                    print("Failed to fetch response")
                }
            }
        }
    }
    
    /**
     Get a list of the playlists owned or followed by a Spotify user.
     
     - parameter artistId: The Spotify ID for the artist.
     - parameter completion: Execute task to return requested Artist's playlist.
     */
    
    public func getPlaylists(for id: String, completion: @escaping ([Playlist]) -> ()) {
        Service.sharedInstance.spotifyService.getToken { (token) in
            let headers: HTTPHeaders = ["Authorization": "Bearer \(token)"]
            
            Alamofire.request("https://api.spotify.com/v1/users/\(id)/playlists", headers: headers).responseJSON { response in
                if let data = response.data {
                    do {
                        let json = try JSON(data: data)
                        let playlistsObject = json["items"].arrayValue
                        let playlists = playlistsObject.map({Playlist(json: $0)})
                        completion(playlists)
                    } catch {
                        print("Failed to pass recently played json response object: \(error)")
                    }
                } else {
                    print("Failed to fetch response")
                }
            }
        }
    }
    
}
