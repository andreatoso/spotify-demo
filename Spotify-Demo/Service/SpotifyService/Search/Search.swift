//
//  Search.swift
//  Spotify-Demo
//
//  Created by Andrea Toso on 21/09/18.
//  Copyright © 2018 Andrea Toso. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

extension SpotifyService {
    
    public func search(query: String, completion: @escaping (SearchResult) -> ()) {
        Service.sharedInstance.spotifyService.getToken { (token) in
            guard let query = query.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) else {return}
            let headers: HTTPHeaders = ["Authorization": "Bearer \(token)"]
            
            Alamofire.request("https://api.spotify.com/v1/search?q=\(query)&type=artist%2Ctrack%2Calbum%2Cplaylist&limit=5", headers: headers).responseJSON { response in
                if let data = response.data {
                    do {
                        let json = try JSON(data: data)
                        completion(self.handleResponse(json: json))
                    } catch {
                        print("Failed to pass recently played json response object: \(error)")
                    }
                } else {
                    print("Failed to fetch response")
                }
            }
        }
    }
    
    fileprivate func handleResponse(json: JSON) -> SearchResult {
        let artists = getResults(json: json, object: "artists", resultType: Artist.self)
        let tracks = getResults(json: json, object: "tracks", resultType: Track.self)
        let albums = getResults(json: json, object: "albums", resultType: Album.self)
        let playlists = getResults(json: json, object: "playlists", resultType: Playlist.self)
        let arr: [RelevantResult] = [.artists(artists), .tracks(tracks)]
        let relevantResult = findRelevantResult(results: arr)
        let result = SearchResult(relevantResult: relevantResult, artists: artists, tracks: tracks, albums: albums, playlists: playlists)
        return result
    }
    
    fileprivate func getResults<T: Mappable>(json: JSON, object: String, resultType: T.Type) -> [T] {
        let types = json[object]
        let typesItems = types["items"].arrayValue
        return typesItems.map({resultType.init(json: $0)})
    }
    
    fileprivate func findRelevantResult(results: [RelevantResult]) -> SearchRelevantResult? {
        var resultArray: [SearchRelevantResult] = []
        
        for result in results {
            switch result {
            case .artists(let artists):
                _ = artists.map({resultArray.append(SearchRelevantResult(id: $0.id, type: .artist, popularity: $0.popularity))})
            case .tracks(let tracks):
                _ = tracks.map({resultArray.append(SearchRelevantResult(id: $0.id, type: .track, popularity: $0.popularity))})
            default:
                break
            }
        }
        
        let sorted = resultArray.sorted(by: {$0.popularity > $1.popularity})
        if !sorted.isEmpty {
            guard let relevantResult = sorted.first else {return nil}
            return relevantResult
        }
        return nil
    }
}
