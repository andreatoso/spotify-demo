//
//  spotifyService.swift
//  Spotify-Demo
//
//  Created by Andrea Toso on 19/09/18.
//  Copyright © 2018 Andrea Toso. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

struct SpotifyService {
    
    internal let serviceConfig = SpotifyServiceConfig()
    
    fileprivate func generateAccessToken(completion: @escaping (String) -> ()) {
        
        let headers: HTTPHeaders = ["Authorization": "Basic \(serviceConfig.basicToken)"]
        let parameters: Parameters = ["grant_type": "refresh_token", "refresh_token": serviceConfig.refreshToken]
        
        Alamofire.request("https://accounts.spotify.com/api/token", method: .post, parameters: parameters, encoding: URLEncoding.httpBody, headers: headers).responseJSON { (response) in
            if let data = response.data {
                do {
                    let json = try JSON(data: data)
                    if let token = json["access_token"].string {
                        let tokenDictionary = SpotifyToken(token: token, date: Date())
                        UserDefaults.standard.set(tokenDictionary.dictionary, forKey: self.serviceConfig.spotifyTokenKey)
                        completion(token)
                    } else {
                        print("Failed to fetch access_token property")
                    }
                } catch {
                    print("Failed to pass json response object: \(error)")
                }
            } else {
                print("Failed to fetch token response")
            }
        }
    }
    
    func getToken(completion: @escaping (String) -> ()) {
        if let spotifyToken = UserDefaults.standard.value(forKey: serviceConfig.spotifyTokenKey) as? [String: Any] {
            
            let tokenDate = spotifyToken["date"] as! Date
            if let interval = Calendar.current.dateComponents([.second], from: tokenDate, to: Date()).second {
                if interval > 3600 {
                    generateAccessToken { (token) in
                        completion(token)
                    }
                } else {
                    completion(spotifyToken["token"] as! String)
                }
            } else {
                completion("Invalud Spotify token interval")
            }
            
        } else {
            generateAccessToken { (token) in
                completion(token)
            }
        }
    }
}
