//
//  SpotifyReceltyPlayed.swift
//  Spotify-Demo
//
//  Created by Andrea Toso on 20/09/18.
//  Copyright © 2018 Andrea Toso. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

extension SpotifyService {
    
    /**
     Get tracks from the current user’s recently played tracks.
     
     - parameter completion: Execute task to return an array containing user’s recently played tracks.
     */
    
    public func getRecentlyPlayed(completion: @escaping ([Track]) -> ()) {
        Service.sharedInstance.spotifyService.getToken { (token) in
            let headers: HTTPHeaders = ["Authorization": "Bearer \(token)",]
            Alamofire.request("https://api.spotify.com/v1/me/player/recently-played?limit=10", headers: headers).responseJSON { response in
                if let data = response.data {
                    do {
                        let json = try JSON(data: data)
                        completion(self.handleResponse(json: json))
                    } catch {
                        print("Failed to pass recently played json response object: \(error)")
                    }
                } else {
                    print("Failed to fetch response")
                }
            }
        }
    }
    
    fileprivate func handleResponse(json: JSON) -> [Track] {
        var recentlyPlayed: [String: Track] = [:]
        let items = json["items"].arrayValue
        
        for item in items {
            let trackObject = item["track"]
            let playedAt = item["played_at"].stringValue
            if !recentlyPlayed.contains(where: { (element) -> Bool in
                return element.value.id == Track(json: trackObject).id
            }) {
                recentlyPlayed[playedAt] = Track(json: trackObject)
            }
        }
        
        let sorted = recentlyPlayed.sorted(by: {$0.0 > $1.0})
        var array: [Track] = []
        for (_, value) in sorted {
            array.append(value)
        }
        return array
    }
}
