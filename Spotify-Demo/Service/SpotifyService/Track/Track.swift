//
//  Track.swift
//  Spotify-Demo
//
//  Created by Andrea Toso on 21/09/18.
//  Copyright © 2018 Andrea Toso. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

extension SpotifyService {
    
    /**
     Get Spotify catalog information for a single track identified by its unique Spotify ID.
     
     - parameter id: The Spotify ID for the track.
     - parameter completion: Execute task to return requested track.
     */
    
    public func getTrack(id: String, completion: @escaping (Track) -> ()) {
        Service.sharedInstance.spotifyService.getToken { (token) in
            let headers: HTTPHeaders = ["Authorization": "Bearer \(token)"]
            Alamofire.request("https://api.spotify.com/v1/tracks/\(id)", headers: headers).responseJSON { response in
                if let data = response.data {
                    do {
                        let json = try JSON(data: data)
                        completion(Track(json: json))
                    } catch {
                        print("Failed to pass recently played json response object: \(error)")
                    }
                } else {
                    print("Failed to fetch response")
                }
            }
        }
    }
    
}
