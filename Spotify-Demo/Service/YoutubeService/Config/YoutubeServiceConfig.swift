//
//  YoutubeServiceConfig.swift
//  Spotify-Demo
//
//  Created by Andrea Toso on 01/10/2018.
//  Copyright © 2018 Andrea Toso. All rights reserved.
//

import Foundation

struct YoutubeServiceConfig {
    
    let clientId = "713482878182-s1vm5afs65kns1v2he5946o4ej26rd7b.apps.googleusercontent.com"
    let clientSecret = "a9MXvLBC01WKPHKmDPstjZa2"
    let youtubeTokenKey = "youtubeTokenKey"
    let refreshToken = "1/0CszHgX5kOJYi4VXr4XKylrVcD0zwBmmmlTf8f8sxGg"
    
}

struct YoutubeToken: Codable {
    let token: String
    let date: Date
    
    var dictionary: [String: Any] {
        return ["token" : token, "date" : date]
    }
}
