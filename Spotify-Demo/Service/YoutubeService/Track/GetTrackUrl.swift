//
//  GetTrack.swift
//  Spotify-Demo
//
//  Created by Andrea Toso on 04/10/2018.
//  Copyright © 2018 Andrea Toso. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

extension YoutubeService {
    
    /**
     Get Spotify catalog information for a single artist identified by their unique Spotify ID.
     
     - parameter id: The Spotify ID for the artist.
     - parameter completion: Execute task to return requested Artist Profile.
     */
    
    internal func getTrackUrl(id: String, completion: @escaping (String) -> ()) {
        Alamofire.request("https://spotify-demo-api.herokuapp.com/api?id=\(id)").responseJSON { response in
            if let data = response.data {
                do {
                    let json = try JSON(data: data)
                    completion(self.handleResponse(json: json))
                } catch {
                    print("Failed to pass recently played json response object: \(error)")
                }
            } else {
                print("Failed to fetch response")
            }
        }
    }
    
    fileprivate func handleResponse(json: JSON) -> String {
        let streams = json["stream"].arrayValue
        if let stream = streams.first {
            return stream["url"].stringValue
        } else {
            print("Failed to get youtube video url")
            return ""
        }
    }
    
}
