//
//  Track.swift
//  Spotify-Demo
//
//  Created by Andrea Toso on 01/10/2018.
//  Copyright © 2018 Andrea Toso. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

extension YoutubeService {
    
    /**
     Get Spotify catalog information for a single artist identified by their unique Spotify ID.
     
     - parameter id: The Spotify ID for the artist.
     - parameter completion: Execute task to return requested Artist Profile.
     */
    
    public func search(query: String, completion: @escaping (String) -> ()) {
        Service.sharedInstance.youtubeService.getToken { (token) in
            guard let query = "\(query) lyrics".addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) else {return}
            
            Alamofire.request("https://www.googleapis.com/youtube/v3/search?q=\(query)&order=relevance&maxResults=1&part=snippet&type=video&access_token=\(token)").responseJSON { response in
                if let data = response.data {
                    do {
                        let json = try JSON(data: data)
                        self.handleResponse(json: json, completion: { (url) in
                            completion(url)
                        })
                    } catch {
                        print("Failed to pass recently played json response object: \(error)")
                    }
                } else {
                    print("Failed to fetch response")
                }
            }
        }
    }
    
    fileprivate func handleResponse(json: JSON, completion: @escaping (String) -> ()) {
        getTrackUrl(id: getId(json: json)) { (url) in
            completion(url)
        }
    }
    
    fileprivate func getId(json: JSON) -> String {
        let items = json["items"].arrayValue
        guard let item = items.first else {
            print("Failed to fetch youtube video id")
            return ""
        }
        let id = item["id"]
        return id["videoId"].stringValue
    }
    
}
