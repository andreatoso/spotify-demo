//
//  YoutubeService.swift
//  Spotify-Demo
//
//  Created by Andrea Toso on 01/10/2018.
//  Copyright © 2018 Andrea Toso. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

struct YoutubeService {
    
    internal let serviceConfig = YoutubeServiceConfig()
    
    fileprivate func generateAccessToken(completion: @escaping (String) -> ()) {
        
        let headers: HTTPHeaders = ["Content-Type": "application/x-www-form-urlencoded"]
        let parameters: Parameters = [
            "grant_type": "refresh_token",
            "refresh_token": serviceConfig.refreshToken,
            "client_id": serviceConfig.clientId,
            "client_secret": serviceConfig.clientSecret
        ]
        
        Alamofire.request("https://www.googleapis.com/oauth2/v4/token", method: .post, parameters: parameters, encoding: URLEncoding.httpBody, headers: headers).responseJSON { (response) in
            if let data = response.data {
                do {
                    let json = try JSON(data: data)
                    if let token = json["access_token"].string {
                        let tokenDictionary = YoutubeToken(token: token, date: Date())
                        UserDefaults.standard.set(tokenDictionary.dictionary, forKey: self.serviceConfig.youtubeTokenKey)
                        completion(token)
                    } else {
                        print("Failed to fetch access_token property")
                    }
                } catch {
                    print("Failed to pass json response object: \(error)")
                }
            } else {
                print("Failed to fetch token response")
            }
        }
    }
    
    func getToken(completion: @escaping (String) -> ()) {
        if let youtubeToken = UserDefaults.standard.value(forKey: serviceConfig.youtubeTokenKey) as? [String: Any] {
            
            let tokenDate = youtubeToken["date"] as! Date
            if let interval = Calendar.current.dateComponents([.second], from: tokenDate, to: Date()).second {
                if interval > 3600 {
                    generateAccessToken { (token) in
                        completion(token)
                    }
                } else {
                    completion(youtubeToken["token"] as! String)
                }
            } else {
                completion("Invalud Youtube token interval")
            }
            
        } else {
            generateAccessToken { (token) in
                completion(token)
            }
        }
    }
}
