//
//  CALayer.swift
//  Spotify-Demo
//
//  Created by Andrea Toso on 09/09/18.
//  Copyright © 2018 Andrea Toso. All rights reserved.
//

import UIKit

extension CALayer {
    
    /**
     Return image for this CALayer.
     */
    
    public func image() -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(self.frame.size, false, 0)
        if let aContext = UIGraphicsGetCurrentContext() {
            self.render(in: aContext)
        }
        let outputImage: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return outputImage
    }
    
}
