//
//  Font.swift
//  Spotify-Demo
//
//  Created by Andrea Toso on 20/08/18.
//  Copyright © 2018 Andrea Toso. All rights reserved.
//

import Foundation

struct Font {
    static var Black: String { return "CircularStd-Black" }
    static var BlackItalic: String  { return "CircularStd-BlackItalic" }
    static var Bold: String  { return "CircularStd-Bold" }
    static var BoldItalic: String  { return "CircularStd-BoldItalic" }
    static var Book: String  { return "CircularStd-Book" }
    static var BookItalic: String  { return "CircularStd-BookItalic" }
    static var Medium: String  { return "CircularStd-Medium" }
    static var MediumItalic: String  { return "CircularStd-MediumItalic" }
}
