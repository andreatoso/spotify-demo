//
//  String.swift
//  Spotify-Demo
//
//  Created by Andrea Toso on 29/09/18.
//  Copyright © 2018 Andrea Toso. All rights reserved.
//

import Foundation

extension String {
    
    public var decimalFormat: String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.locale = Locale(identifier: "en_US")
        let amount = Int(self)
        if let string = formatter.string(for: amount) {
            return string.replacingOccurrences(of: ",", with: ".")
        }
        return self
    }
    
}
