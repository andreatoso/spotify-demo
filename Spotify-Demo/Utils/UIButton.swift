//
//  UIButton.swift
//  Spotify-Demo
//
//  Created by Andrea Toso on 03/09/18.
//  Copyright © 2018 Andrea Toso. All rights reserved.
//

import UIKit

class SpotifyButton: UIButton {
    
    override func setTitle(_ title: String?, for state: UIControl.State) {
        guard let length = title?.count else {return}
        guard let title = title else {return}
        let attributedString = NSMutableAttributedString(string: title)
        attributedString.addAttribute(kCTKernAttributeName as NSAttributedString.Key, value: 1.2, range: NSRange(location: 0, length: length))
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.white, range: NSRange(location: 0, length: length))
        self.setAttributedTitle(attributedString, for: .normal)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)

        self.titleLabel?.font = UIFont.init(name: Font.Bold, size: 14)
        self.backgroundColor = Color.Green
        self.layer.cornerRadius = 25
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
