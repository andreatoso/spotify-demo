//
//  UIColor.swift
//  Spotify-Demo
//
//  Created by Andrea Toso on 20/08/18.
//  Copyright © 2018 Andrea Toso. All rights reserved.
//

import UIKit

extension UIColor {
    
    convenience public init(r: CGFloat, g: CGFloat, b: CGFloat, a: CGFloat) {
        self.init(red: r/255, green: g/255, blue: b/255, alpha: a)
    }
    
}

struct Color {
    static var Black: UIColor { return UIColor(r: 17, g: 18, b: 19, a: 1) }
    static var Gray: UIColor { return UIColor(r: 39, g: 40, b: 41, a: 1) }
    static var LightGray: UIColor { return UIColor(r: 179, g: 179, b: 179, a: 1) }
    static var Green: UIColor { return UIColor(r: 25, g: 185, b: 86, a: 1) }
}
