//
//  ArtistAlbumCell.swift
//  Spotify-Demo
//
//  Created by Andrea Toso on 10/09/18.
//  Copyright © 2018 Andrea Toso. All rights reserved.
//

import UIKit

class ArtistAlbumCell: UICollectionViewCell, UIGestureRecognizerDelegate {
    
    var artistAlbumModel: Album?
    var preventSetup = false
    var indexPath: IndexPath?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupCellPosition(indexPath: IndexPath) {
        self.indexPath = indexPath
        setupViews()
    }
    
    func setupArtistAlbum(album: Album) {
        artistAlbumModel = album
        setupInfo()
        setupViews()
    }
    
    let image: CachedImageView = {
        let iv = CachedImageView()
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    let albumName: UILabel = {
        let label = UILabel()
        label.text = "beerbongs & bentleys"
        label.textColor = UIColor.white
        label.font = UIFont(name: Font.Bold, size: 14)
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let albumInfo: UILabel = {
        let label = UILabel()
        label.text = "18 BRANI • 2018".uppercased()
        label.font = UIFont.init(name: Font.Book, size: 11.5)
        label.textColor = Color.LightGray
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    fileprivate func setupViews() {
        if !preventSetup {
            backgroundColor = Color.Black
            
            addSubview(image)
            guard let index = indexPath else {return}
            
            if index.item%2 == 0 {
                image.anchor(topAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, topConstant: 11, leftConstant: 15, bottomConstant: 0, rightConstant: 7.5, widthConstant: 0, heightConstant: 0)
                image.heightAnchor.constraint(equalTo: image.widthAnchor).isActive = true
            } else {
                image.anchor(topAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, topConstant: 11, leftConstant: 7.5, bottomConstant: 0, rightConstant: 15, widthConstant: 0, heightConstant: 0)
                image.heightAnchor.constraint(equalTo: image.widthAnchor).isActive = true
            }
            preventSetup = true
            
            addSubview(albumName)
            albumName.centerXAnchor.constraint(equalTo: image.centerXAnchor).isActive = true
            albumName.topAnchor.constraint(equalTo: image.bottomAnchor, constant: 5).isActive = true
            albumName.widthAnchor.constraint(equalTo: widthAnchor, constant: -30).isActive = true
            
            addSubview(albumInfo)
            albumInfo.centerXAnchor.constraint(equalTo: image.centerXAnchor).isActive = true
            albumInfo.topAnchor.constraint(equalTo: albumName.bottomAnchor, constant: 1.25).isActive = true
            
            let gesture = UILongPressGestureRecognizer(target: self, action: #selector(handleGesture))
            gesture.delegate = self
            gesture.minimumPressDuration = 0
            gesture.cancelsTouchesInView = false
            addGestureRecognizer(gesture)
        }
    }
    
    fileprivate func setupInfo() {
        guard let album = artistAlbumModel else {return}
        guard let albumYear = album.releaseDate.split(separator: "-").first else {return}
        
        image.loadImage(urlString: album.imageUrl)
        albumName.text = album.name
        
        let attribute1 = [NSAttributedString.Key.kern: 1]
        let attribute2 = [NSAttributedString.Key.font: UIFont.init(name: Font.Book, size: 10.5) as Any, NSAttributedString.Key.kern: 1]
        let baseString = NSMutableAttributedString(string: "\(album.numberOfTracks)", attributes: attribute1)
        let labelString = NSMutableAttributedString(string: " BRANI • ", attributes: attribute2)
        let finalString = NSMutableAttributedString(string: "\(albumYear)", attributes: attribute1)
        baseString.append(labelString)
        baseString.append(finalString)
        albumInfo.attributedText = baseString
    }
    
    @objc fileprivate func handleGesture(gesture: UILongPressGestureRecognizer) {
        switch gesture.state {
        case .began:
            self.subviews.forEach{($0.alpha = 0.9)}
        case .cancelled, .ended:
            self.subviews.forEach({$0.alpha = 1})
        default:
            break
        }
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
}
