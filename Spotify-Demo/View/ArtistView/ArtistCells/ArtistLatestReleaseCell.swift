//
//  ArtistLatestReleasesCell.swift
//  Spotify-Demo
//
//  Created by Andrea Toso on 04/09/18.
//  Copyright © 2018 Andrea Toso. All rights reserved.
//

import UIKit

class ArtistLatestReleaseCell: ArtistBaseCell {
    
    var album: Album?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupLatestRelease(album: Album) {
        self.album = album
        setupInfo()
    }
    
    let image: CachedImageView = {
        let iv = CachedImageView()
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    let name: UILabel = {
        let label = UILabel()
        label.text = "beerbongs & bentleys"
        label.font = UIFont.init(name: Font.Medium, size: 16.5)
        label.textColor = UIColor.white
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let info: UILabel = {
        let label = UILabel()
        label.text = "18 BRANI • 2018".uppercased()
        label.font = UIFont.init(name: Font.Book, size: 14)
        label.textColor = Color.LightGray
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    fileprivate func setupViews() {
        addSubview(image)
        image.anchor(topAnchor, left: leftAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 15, bottomConstant: 0, rightConstant: 0, widthConstant: frame.height, heightConstant: frame.height)
        
        addSubview(name)
        name.leftAnchor.constraint(equalTo: image.rightAnchor, constant: 15).isActive = true
        name.rightAnchor.constraint(equalTo: rightArrow.leftAnchor, constant: -15).isActive = true
        name.bottomAnchor.constraint(equalTo: centerYAnchor, constant: 1).isActive = true
        
        addSubview(info)
        info.topAnchor.constraint(equalTo: centerYAnchor, constant: 2.5).isActive = true
        info.rightAnchor.constraint(equalTo: rightArrow.leftAnchor, constant: -8).isActive = true
        info.leftAnchor.constraint(equalTo: name.leftAnchor).isActive = true
    }
    
    fileprivate func setupInfo() {
        guard let album = album else {return}
        let albumYear = "\(album.releaseDate.split(separator: "-")[0])"
        
        image.loadImage(urlString: album.imageUrl)
        name.text = album.name
        let baseString = NSMutableAttributedString(string: "\(album.numberOfTracks)")
        let labelString = NSAttributedString(string: " BRANI • ", attributes: [NSAttributedString.Key.font: UIFont.init(name: Font.Book, size: 12.75) as Any])
        let finalString = NSMutableAttributedString(string: "\(albumYear)")
        baseString.append(labelString)
        baseString.append(finalString)
        info.attributedText = baseString
    }
    
}
