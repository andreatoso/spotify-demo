//
//  ArtistPlaylistCell.swift
//  Spotify-Demo
//
//  Created by Andrea Toso on 04/09/18.
//  Copyright © 2018 Andrea Toso. All rights reserved.
//

import UIKit

class ArtistPlaylistCell: ArtistBaseCell {
    
    var playlist: Playlist?
    var artist: Artist?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupArtistPlaylist(playlist: Playlist, artist: Artist) {
        self.playlist = playlist
        self.artist = artist
        setupInfo()
    }
    
    let playlistImage: CachedImageView = {
        let iv = CachedImageView()
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    let playlistName: UILabel = {
        let label = UILabel()
        label.text = "postys playlist"
        label.font = UIFont.init(name: Font.Medium, size: 16.5)
        label.textColor = UIColor.white
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let playlistLabel: UILabel = {
        let label = UILabel()
        label.text = "Playlist"
        label.font = UIFont.init(name: Font.Book, size: 14)
        label.textColor = Color.LightGray
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let roundSizeContainer: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let roundContainer: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.layer.cornerRadius = 14
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let smallProfileImage: CachedImageView = {
        let iv = CachedImageView()
        iv.layer.cornerRadius = 11
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    let artistName: UILabel = {
        let label = UILabel()
        label.text = "Post Malone"
        label.font = UIFont.init(name: Font.Medium, size: 14)
        label.textColor = Color.Black
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    fileprivate func setupViews() {
        addSubview(playlistImage)
        playlistImage.anchor(nil, left: leftAnchor, bottom: bottomAnchor, right: nil, topConstant: 0, leftConstant: 15, bottomConstant: 10, rightConstant: 0, widthConstant: frame.height - 20, heightConstant: frame.height - 20)
        
        addSubview(playlistLabel)
        playlistLabel.leftAnchor.constraint(equalTo: playlistImage.rightAnchor, constant: 15).isActive = true
        playlistLabel.bottomAnchor.constraint(equalTo: playlistImage.bottomAnchor, constant: -3).isActive = true
        
        addSubview(playlistName)
        playlistName.leftAnchor.constraint(equalTo: playlistLabel.leftAnchor).isActive = true
        playlistName.bottomAnchor.constraint(equalTo: playlistLabel.topAnchor, constant: -1).isActive = true
        playlistName.rightAnchor.constraint(equalTo: rightArrow.leftAnchor, constant: -15).isActive = true
        
        addSubview(roundSizeContainer)
        roundSizeContainer.anchor(playlistImage.topAnchor, left: playlistLabel.leftAnchor, bottom: nil, right: rightArrow.leftAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 15, widthConstant: 0, heightConstant: 28)
        
        roundSizeContainer.addSubview(roundContainer)
        addSubview(smallProfileImage)
        smallProfileImage.anchor(playlistImage.topAnchor, left: playlistLabel.leftAnchor, bottom: nil, right: nil, topConstant: 3, leftConstant: 3, bottomConstant: 0, rightConstant: 0, widthConstant: 22, heightConstant: 22)
        
        addSubview(artistName)
        artistName.leftAnchor.constraint(equalTo: smallProfileImage.rightAnchor, constant: 8).isActive = true
        artistName.centerYAnchor.constraint(equalTo: smallProfileImage.centerYAnchor).isActive = true
        
        roundContainer.anchor(playlistImage.topAnchor, left: playlistLabel.leftAnchor, bottom: smallProfileImage.bottomAnchor, right: artistName.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: -3, rightConstant: -3 - 11, widthConstant: 0, heightConstant: 0)
        roundContainer.widthAnchor.constraint(lessThanOrEqualTo: roundSizeContainer.widthAnchor).isActive = true
    }
    
    fileprivate func setupInfo() {
        guard let playlist = self.playlist else {return}
        guard let artist = self.artist else {return}
        
        playlistImage.loadImage(urlString: playlist.imageUrl)
        smallProfileImage.loadImage(urlString: artist.profileImageUrl)
        playlistName.text = playlist.name
        artistName.text = artist.name
    }
    
}
