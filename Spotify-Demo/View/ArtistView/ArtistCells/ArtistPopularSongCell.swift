//
//  ArtistPopularSongCell.swift
//  Spotify-Demo
//
//  Created by Andrea Toso on 05/09/18.
//  Copyright © 2018 Andrea Toso. All rights reserved.
//

import UIKit

class ArtistPopularSongCell: UICollectionViewCell, UIGestureRecognizerDelegate {
    
    var track: Track?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupPopuplarTrack(track: Track) {
        self.track = track
        setupInfo()
        setupViews()
    }
    
    let songNumber: UILabel = {
        let label = UILabel()
        label.text = "1"
        label.font = UIFont.init(name: Font.Medium, size: 17.5)
        label.textColor = Color.LightGray
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let name: UILabel = {
        let label = UILabel()
        label.text = "Shade"
        label.font = UIFont.init(name: Font.Medium, size: 16.5)
        label.textColor = UIColor.white
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let artist: UILabel = {
        let label = UILabel()
        label.text = "Artista"
        label.font = UIFont.init(name: Font.Book, size: 14)
        label.textColor = Color.LightGray
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let explicitContainer: UIView = {
        let view = UIView()
        view.backgroundColor = Color.LightGray
        view.layer.cornerRadius = 3
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let explicit: UILabel = {
        let label = UILabel()
        label.text = "EXPLICIT"
        label.font = UIFont.init(name: Font.Book, size: 9)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    lazy var dotsContainer: UIButton = {
        let iv = UIButton()
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    lazy var dots: UIButton = {
        let iv = UIButton()
        iv.setImage(UIImage(named: "dots")?.withRenderingMode(.alwaysTemplate), for: .normal)
        iv.imageView?.tintColor = Color.LightGray
        iv.imageView?.contentMode = .scaleAspectFit
        iv.imageEdgeInsets = UIEdgeInsets.init(top: 0, left: 7, bottom: 0, right: 7)
        iv.layer.zPosition = 99999
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    fileprivate func setupViews() {
        backgroundColor = Color.Black
        guard let track = self.track else {return}
        
        addSubview(dotsContainer)
        dotsContainer.anchorCenterYToSuperview()
        dotsContainer.rightAnchor.constraint(equalTo: rightAnchor, constant: -15).isActive = true
        dotsContainer.heightAnchor.constraint(equalToConstant: 30).isActive = true
        dotsContainer.widthAnchor.constraint(equalToConstant: 30).isActive = true
        
        addSubview(name)
        name.leftAnchor.constraint(equalTo: leftAnchor, constant: 27 + 27 + 4).isActive = true
        name.rightAnchor.constraint(equalTo: dotsContainer.leftAnchor, constant: -15).isActive = true
        
        addSubview(songNumber)
        songNumber.anchorCenterYToSuperview()
        songNumber.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        songNumber.rightAnchor.constraint(equalTo: name.leftAnchor).isActive = true
        
        addSubview(explicitContainer)
        explicitContainer.anchor(centerYAnchor, left: name.leftAnchor, bottom: nil, right: nil, topConstant: 5, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 46, heightConstant: 12.5)
        
        explicitContainer.addSubview(explicit)
        explicit.anchorCenterSuperview()
        
        addSubview(artist)
        artist.topAnchor.constraint(equalTo: centerYAnchor, constant: 2.5).isActive = true
        artist.rightAnchor.constraint(equalTo: dotsContainer.leftAnchor, constant: -15).isActive = true
        
        explicitContainer.isHidden = !track.isExplicit
        artist.isHidden = track.artists.isEmpty
        
        if !track.isExplicit && track.artists.isEmpty {
            name.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        } else {
            name.bottomAnchor.constraint(equalTo: centerYAnchor, constant: 1).isActive = true
            
            if !track.artists.isEmpty {
                if !track.isExplicit {
                    artist.leftAnchor.constraint(equalTo: name.leftAnchor).isActive = true
                } else {
                    artist.leftAnchor.constraint(equalTo: explicitContainer.rightAnchor, constant: 8).isActive = true
                }
            }
        }
        
        addSubview(dots)
        dots.anchorCenterYToSuperview()
        dots.rightAnchor.constraint(equalTo: rightAnchor, constant: -15).isActive = true
        dots.heightAnchor.constraint(equalToConstant: 30).isActive = true
        dots.widthAnchor.constraint(equalToConstant: 30).isActive = true
        
        let gesture = UILongPressGestureRecognizer(target: self, action: #selector(handleGesture))
        gesture.delegate = self
        gesture.minimumPressDuration = 0
        gesture.cancelsTouchesInView = false
        addGestureRecognizer(gesture)
    }
    
    fileprivate func setupInfo() {
        guard let track = self.track else {return}
        
        name.text = track.name
        artist.text = ""
        track.artists.forEach { (text) in
            guard let artistText = artist.text else {return}
            artist.text = artistText.isEmpty ? "\(text.name)" : "\(artistText), \(text.name)"
        }
    }
    
    @objc fileprivate func handleGesture(gesture: UILongPressGestureRecognizer) {
        switch gesture.state {
        case .began:
            self.subviews.forEach{($0.alpha = 0.9)}
        case .cancelled, .ended:
            self.subviews.forEach({$0.alpha = 1})
        default:
            break
        }
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        // Resolve bug for "explicit" container
        artist.removeFromSuperview()
    }
    
}
