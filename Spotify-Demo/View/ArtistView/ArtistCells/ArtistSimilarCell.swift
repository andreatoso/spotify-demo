//
//  ArtistSimilarCell.swift
//  Spotify-Demo
//
//  Created by Andrea Toso on 10/09/18.
//  Copyright © 2018 Andrea Toso. All rights reserved.
//

import UIKit

class ArtistSimilarCell: UICollectionViewCell, UIGestureRecognizerDelegate {
    
    var similarArtistModel: [Artist]?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupSimilarArtists(artists: [Artist]) {
        similarArtistModel = artists
        setupInfo()
        setupViews()
    }
    
    let artist: UILabel = {
        let label = UILabel()
        label.text = "Big Sean, J. Cole, Tyga, Jeremih, 2 Chainz, DJ Khaled, Tory Lanez, Future, Bryson Tiller, Meek Mill, Ty Dolla $ign"
        label.textColor = UIColor.white
        label.font = UIFont.init(name: Font.Medium, size: 13.1)
        label.numberOfLines = 2
        
        let attributedString = NSMutableAttributedString(string: label.text!)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 2.1
        paragraphStyle.lineBreakMode = .byTruncatingTail
        attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value: paragraphStyle, range: NSMakeRange(0, attributedString.length))
        label.attributedText = attributedString
        
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    fileprivate func setupViews() {
        addSubview(artist)
        artist.anchor(topAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, topConstant: 7.5, leftConstant: 15, bottomConstant: 0, rightConstant: 15, widthConstant: 0, heightConstant: 0)
        
        let gesture = UILongPressGestureRecognizer(target: self, action: #selector(handleGesture))
        gesture.delegate = self
        gesture.minimumPressDuration = 0
        gesture.cancelsTouchesInView = false
        addGestureRecognizer(gesture)
    }
    
    fileprivate func setupInfo() {
        guard let artists = similarArtistModel else {return}
        var artistStringText = ""
        artists.forEach({ (artistName) in
            artistStringText += "\(artistName.name), "
        })
        
        //Remove ", " characters
        if artistStringText.count > 0 {
            artistStringText.remove(at: artistStringText.index(before: artistStringText.endIndex))
            artistStringText.remove(at: artistStringText.index(before: artistStringText.endIndex))
            artist.text = artistStringText
        }
        
        guard let artistText = artist.text else {return}
        let attributedString = NSMutableAttributedString(string: artistText)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 2.1
        paragraphStyle.lineBreakMode = .byTruncatingTail
        attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value: paragraphStyle, range: NSMakeRange(0, attributedString.length))
        artist.attributedText = attributedString
    }
    
    @objc fileprivate func handleGesture(gesture: UILongPressGestureRecognizer) {
        switch gesture.state {
        case .began:
            self.subviews.forEach{($0.alpha = 0.9)}
        case .cancelled, .ended:
            self.subviews.forEach({$0.alpha = 1})
        default:
            break
        }
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
}
