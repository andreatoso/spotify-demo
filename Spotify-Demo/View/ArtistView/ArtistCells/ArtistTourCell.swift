//
//  ArtistTourCell.swift
//  Spotify-Demo
//
//  Created by Andrea Toso on 10/09/18.
//  Copyright © 2018 Andrea Toso. All rights reserved.
//

import UIKit

class ArtistTourCell: UICollectionViewCell, UIGestureRecognizerDelegate {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let calendarContainer: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let monthContainer: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(r: 204, g: 25, b: 44, a: 1)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let month: UILabel = {
        let label = UILabel()
        label.text = "SET"
        label.textColor = UIColor.white
        label.font = UIFont.init(name: Font.Black, size: 11.1)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let dayNumber: UILabel = {
        let label = UILabel()
        label.text = "4"
        label.textColor = Color.Black
        label.font = UIFont.init(name: Font.Medium, size: 19)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let artists: UILabel = {
        let label = UILabel()
        label.text = "Drake, Migos"
        label.font = UIFont.init(name: Font.Medium, size: 16.5)
        label.textColor = UIColor.white
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let info: UILabel = {
        let label = UILabel()
        label.text = "mar, 19.00 • Centre Bell Centre, Montreal"
        label.font = UIFont.init(name: Font.Book, size: 13)
        label.textColor = Color.LightGray
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    fileprivate func setupViews() {
        addSubview(calendarContainer)
        calendarContainer.anchor(topAnchor, left: leftAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 15, bottomConstant: 0, rightConstant: 0, widthConstant: frame.height, heightConstant: frame.height)
        
        calendarContainer.addSubview(monthContainer)
        monthContainer.anchor(calendarContainer.topAnchor, left: calendarContainer.leftAnchor, bottom: nil, right: calendarContainer.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 17)
        
        monthContainer.addSubview(month)
        month.anchorCenterSuperview()
        
        calendarContainer.addSubview(dayNumber)
        dayNumber.anchorCenterXToSuperview()
        dayNumber.centerYAnchor.constraint(equalTo: calendarContainer.centerYAnchor, constant: 17/2 - 1).isActive = true
        
        addSubview(artists)
        artists.leftAnchor.constraint(equalTo: calendarContainer.rightAnchor, constant: 15).isActive = true
        artists.rightAnchor.constraint(equalTo: rightAnchor, constant: -15).isActive = true
        artists.bottomAnchor.constraint(equalTo: centerYAnchor, constant: 1).isActive = true
        
        addSubview(info)
        info.topAnchor.constraint(equalTo: centerYAnchor, constant: 2.5).isActive = true
        info.rightAnchor.constraint(equalTo: rightAnchor, constant: -15).isActive = true
        info.leftAnchor.constraint(equalTo: artists.leftAnchor).isActive = true
        
        let gesture = UILongPressGestureRecognizer(target: self, action: #selector(handleGesture))
        gesture.delegate = self
        gesture.minimumPressDuration = 0
        gesture.cancelsTouchesInView = false
        addGestureRecognizer(gesture)
    }
    
    @objc fileprivate func handleGesture(gesture: UILongPressGestureRecognizer) {
        switch gesture.state {
        case .began:
            self.subviews.forEach{($0.alpha = 0.9)}
        case .cancelled, .ended:
            self.subviews.forEach({$0.alpha = 1})
        default:
            break
        }
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
}
