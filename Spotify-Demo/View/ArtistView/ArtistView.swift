//
//  ArtistView.swift
//  Spotify-Demo
//
//  Created by Andrea Toso on 02/09/18.
//  Copyright © 2018 Andrea Toso. All rights reserved.
//

import UIKit

class ArtistView: UIView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let shadow: UIView = {
        let shadow = UIView()
        shadow.backgroundColor = .clear
        shadow.layer.shadowColor = UIColor.black.withAlphaComponent(0.9).cgColor
        shadow.layer.shadowOpacity = 0.5
        shadow.layer.shadowRadius = 10
        shadow.layer.shadowOffset = CGSize.zero
        shadow.isHidden = true
        shadow.translatesAutoresizingMaskIntoConstraints = false
        return shadow
    }()
    
    let profileImage: CachedImageView = {
        let image = CachedImageView()
        image.backgroundColor = .clear
        image.layer.cornerRadius = 60
        image.translatesAutoresizingMaskIntoConstraints = false
        return image
    }()
    
    let name: UILabel = {
        let label = UILabel()
        label.text = "Post Malone"
        label.textColor = UIColor.white
        label.font = UIFont.init(name: Font.Bold, size: 25)
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let followers: UILabel = {
        let label = UILabel()
        label.text = "36.069.873 FOLLOWERS"
        label.textColor = Color.LightGray
        label.font = UIFont.init(name: Font.Medium, size: 11)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    fileprivate func setupViews() {
        addSubview(shadow)
        
        addSubview(profileImage)
        profileImage.anchorCenterXToSuperview()
        profileImage.topAnchor.constraint(equalTo: topAnchor).isActive = true
        profileImage.heightAnchor.constraint(equalToConstant: 120).isActive = true
        profileImage.widthAnchor.constraint(equalToConstant: 120).isActive = true
        
        shadow.anchorCenterXToSuperview()
        shadow.topAnchor.constraint(equalTo: topAnchor).isActive = true
        shadow.heightAnchor.constraint(equalToConstant: 120).isActive = true
        shadow.widthAnchor.constraint(equalToConstant: 120).isActive = true
        
        addSubview(name)
        name.anchorCenterXToSuperview()
        name.topAnchor.constraint(equalTo: profileImage.bottomAnchor, constant: 10).isActive = true
        name.widthAnchor.constraint(equalTo: widthAnchor, constant: -50).isActive = true
        
        addSubview(followers)
        followers.anchorCenterXToSuperview()
        followers.topAnchor.constraint(equalTo: name.bottomAnchor, constant: 10).isActive = true
    }
    
    func formatFollowersStyle(for type: LibraryType) {
        switch type {
        case .album:
            followers.font = UIFont.init(name: Font.Medium, size: 13)
        case .playlist, .artist:
            guard let length = followers.text?.count else {return}
            guard let title = followers.text else {return}
            let attributedString = NSMutableAttributedString(string: title)
            attributedString.addAttribute(kCTKernAttributeName as NSAttributedString.Key, value: 1.05, range: NSRange(location: 0, length: length))
            followers.attributedText = attributedString
        }
    }
    
}
