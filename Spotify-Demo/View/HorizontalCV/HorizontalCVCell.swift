//
//  HorizontalCVCell.swift
//  Spotify-Demo
//
//  Created by Andrea Toso on 20/08/18.
//  Copyright © 2018 Andrea Toso. All rights reserved.
//

import UIKit

class HorizontalCVCell: UICollectionViewCell, UIGestureRecognizerDelegate {
    
    var track: Track? { didSet {setupTrack()} }
    var playlist: Playlist? { didSet {setupPlaylist()} }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let songCover: CachedImageView = {
        let view = CachedImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let songTitle: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont(name: Font.Bold, size: 14)
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    fileprivate func setupViews() {
        addSubview(songCover)
        songCover.anchor(topAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        songCover.heightAnchor.constraint(equalTo: songCover.widthAnchor).isActive = true
        
        addSubview(songTitle)
        songTitle.topAnchor.constraint(equalTo: songCover.bottomAnchor, constant: 8).isActive = true
        songTitle.widthAnchor.constraint(equalTo: widthAnchor).isActive = true
    
        let gesture = UILongPressGestureRecognizer(target: self, action: #selector(handleGesture))
        gesture.delegate = self
        gesture.minimumPressDuration = 0
        gesture.cancelsTouchesInView = false
        addGestureRecognizer(gesture)
    }
    
    @objc fileprivate func handleGesture(gesture: UILongPressGestureRecognizer) {
        switch gesture.state {
        case .began:
            self.subviews.forEach{($0.alpha = 0.9)}
        case .cancelled, .ended:
            self.subviews.forEach({$0.alpha = 1})
        default:
            break
        }
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
}

extension HorizontalCVCell {
    
    // MARK: Track
    internal func setupTrack() {
        guard let imageUrl = track?.album.imageUrl else {return}
        guard let songTitle = track?.name else {return}
        self.songCover.loadImage(urlString: imageUrl)
        self.songTitle.text = songTitle
    }
    
    // MARK: Playlist
    internal func setupPlaylist() {
        guard let imageUrl = playlist?.imageUrl else {return}
        self.songCover.loadImage(urlString: imageUrl) {
            self.songCover.image = self.songCover.image
        }
        
        var description = ""
        guard let tracks = playlist?.tracks else {return}
        for track in tracks {
            let artists = track.artists
            for artist in artists {
                description += "\(artist.name), "
            }
        }
        
        self.songTitle.text = description
    }
    
}
