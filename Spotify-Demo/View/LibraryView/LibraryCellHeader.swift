//
//  LibraryCellHeader.swift
//  Spotify-Demo
//
//  Created by Andrea Toso on 11/09/18.
//  Copyright © 2018 Andrea Toso. All rights reserved.
//

import UIKit

class LibraryCellHeader: UICollectionViewCell {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let title: UILabel = {
        let label = UILabel()
        label.text = "Download"
        label.textColor = UIColor.white
        label.font = UIFont(name: Font.Bold, size: 16.5)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let switchController: UISwitch = {
        let uis = UISwitch()
        uis.tintColor = UIColor(r: 66, g: 66, b: 66, a: 1)
        uis.translatesAutoresizingMaskIntoConstraints = false
        return uis
    }()
    
    fileprivate func setupViews() {
        backgroundColor = Color.Black
        
        addSubview(title)
        title.leftAnchor.constraint(equalTo: leftAnchor, constant: 15).isActive = true
        title.centerYAnchor.constraint(equalTo: centerYAnchor, constant: 14).isActive = true
        
        addSubview(switchController)
        switchController.rightAnchor.constraint(equalTo: rightAnchor, constant: -15).isActive = true
        switchController.centerYAnchor.constraint(equalTo: title.centerYAnchor).isActive = true
    }
    
}
