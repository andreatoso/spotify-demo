//
//  SearchCellFooter.swift
//  Spotify-Demo
//
//  Created by Andrea Toso on 24/08/18.
//  Copyright © 2018 Andrea Toso. All rights reserved.
//

import UIKit

class SearchCellFooter: UICollectionViewCell, UIGestureRecognizerDelegate {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let seeMore: UILabel = {
        let label = UILabel()
        label.text = "Visualizza altro"
        label.textColor = UIColor.white
        label.font = UIFont.init(name: Font.Medium, size: 16.5)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let rightArrow: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "right_arrow")?.withRenderingMode(.alwaysTemplate)
        iv.tintColor = Color.LightGray
        iv.contentMode = .scaleAspectFit
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    fileprivate func setupViews() {
        addSubview(seeMore)
        seeMore.anchorCenterYToSuperview()
        seeMore.leftAnchor.constraint(equalTo: leftAnchor, constant: 15).isActive = true
        
        addSubview(rightArrow)
        rightArrow.anchorCenterYToSuperview()
        rightArrow.anchor(nil, left: nil, bottom: nil, right: rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 15, widthConstant: 15, heightConstant: 15)
        
        let gesture = UILongPressGestureRecognizer(target: self, action: #selector(handleGesture))
        gesture.delegate = self
        gesture.minimumPressDuration = 0
        gesture.cancelsTouchesInView = false
        addGestureRecognizer(gesture)
    }
    
    @objc fileprivate func handleGesture(gesture: UILongPressGestureRecognizer) {
        switch gesture.state {
        case .began:
            self.subviews.forEach{($0.alpha = 0.9)}
        case .cancelled, .ended:
            self.subviews.forEach({$0.alpha = 1})
        default:
            break
        }
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
}
