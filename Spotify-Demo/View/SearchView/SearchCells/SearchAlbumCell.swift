//
//  ArtistSearchCell.swift
//  Spotify-Demo
//
//  Created by Andrea Toso on 28/08/18.
//  Copyright © 2018 Andrea Toso. All rights reserved.
//

import UIKit

class SearchAlbumCell: SearchBaseCell {
    
    var searchAlbumModel: Album?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupAlbumResult(model: Album) {
        searchAlbumModel = model
        setupAlbumResultViews()
        setupInfo()
    }
    
    let name: UILabel = {
        let label = UILabel()
        label.text = "Shade"
        label.font = UIFont.init(name: Font.Medium, size: 16.5)
        label.textColor = UIColor.white
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let artist: UILabel = {
        let label = UILabel()
        label.text = "Artista"
        label.font = UIFont.init(name: Font.Book, size: 14)
        label.textColor = Color.LightGray
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    fileprivate func setupAlbumResultViews() {
        profileImage.layer.cornerRadius = 0
        
        addSubview(name)
        name.leftAnchor.constraint(equalTo: profileImage.rightAnchor, constant: 15).isActive = true
        name.rightAnchor.constraint(equalTo: rightArrow.leftAnchor, constant: -15).isActive = true
        name.bottomAnchor.constraint(equalTo: centerYAnchor, constant: 1).isActive = true
        
        addSubview(artist)
        artist.topAnchor.constraint(equalTo: centerYAnchor, constant: 2.5).isActive = true
        artist.rightAnchor.constraint(equalTo: rightArrow.leftAnchor, constant: -8).isActive = true
        artist.leftAnchor.constraint(equalTo: name.leftAnchor).isActive = true
        
        guard let artists = searchAlbumModel?.artists else {return}
        artist.isHidden = artists.isEmpty
    }
    
    fileprivate func setupInfo() {
        guard let album = searchAlbumModel else {return}
        let albumCover = album.imageUrl
        let albumName = album.name
        let artists = album.artists
        
        profileImagePlaceholder.image = UIImage(named: "album_placeholder")
        profileImagePlaceholder.isHidden = !albumCover.isEmpty
        profileImage.loadImage(urlString: albumCover)
        name.text = albumName
        artist.text = ""
        artists.forEach { (text) in
            guard let artistText = artist.text else {return}
            artist.text = artistText.isEmpty ? "\(text.name)" : "\(artistText), \(text.name)"
        }
    }
    
}
