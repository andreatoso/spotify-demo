//
//  ArtistSearchCell.swift
//  Spotify-Demo
//
//  Created by Andrea Toso on 25/08/18.
//  Copyright © 2018 Andrea Toso. All rights reserved.
//

import UIKit

class SearchBaseCell: UICollectionViewCell, UIGestureRecognizerDelegate {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let profileImage: CachedImageView = {
        let iv = CachedImageView()
        iv.backgroundColor = UIColor(r: 39, g: 40, b: 41, a: 1)
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    let profileImagePlaceholder: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "artist_placeholder")
        iv.isHidden = true
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    let rightArrow: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "right_arrow")?.withRenderingMode(.alwaysTemplate)
        iv.tintColor = Color.LightGray
        iv.contentMode = .scaleAspectFit
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    fileprivate func setupViews() {
        addSubview(profileImage)
        profileImage.anchor(topAnchor, left: leftAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 15, bottomConstant: 0, rightConstant: 0, widthConstant: frame.height, heightConstant: frame.height)
        profileImage.layer.cornerRadius = frame.height/2
        
        profileImage.addSubview(profileImagePlaceholder)
        profileImagePlaceholder.fillSuperview()
        
        addSubview(rightArrow)
        rightArrow.anchorCenterYToSuperview()
        rightArrow.anchor(nil, left: nil, bottom: nil, right: rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 15, widthConstant: 15, heightConstant: 15)
        
        let gesture = UILongPressGestureRecognizer(target: self, action: #selector(handleGesture))
        gesture.delegate = self
        gesture.minimumPressDuration = 0
        gesture.cancelsTouchesInView = false
        addGestureRecognizer(gesture)
    }
    
    @objc fileprivate func handleGesture(gesture: UILongPressGestureRecognizer) {
        switch gesture.state {
        case .began:
            self.subviews.forEach{($0.alpha = 0.9)}
        case .cancelled, .ended:
            self.subviews.forEach({$0.alpha = 1})
        default:
            break
        }
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
}
