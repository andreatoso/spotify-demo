//
//  RelevantSearchCell.swift
//  Spotify-Demo
//
//  Created by Andrea Toso on 25/08/18.
//  Copyright © 2018 Andrea Toso. All rights reserved.
//

import UIKit

class SearchRelevantCell: SearchBaseCell {
    
    var searchResultModel: SearchRelevantResult?
    var gesture: UITapGestureRecognizer?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let name: UILabel = {
        let label = UILabel()
        label.text = "Shade"
        label.font = UIFont.init(name: Font.Medium, size: 16.5)
        label.textColor = UIColor.white
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let resultInfo: UILabel = {
        let label = UILabel()
        label.text = "Artista"
        label.font = UIFont.init(name: Font.Medium, size: 12)
        label.textColor = Color.LightGray
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    fileprivate func setupSearchResultView() {        
        addSubview(name)
        name.leftAnchor.constraint(equalTo: profileImage.rightAnchor, constant: 15).isActive = true
        name.rightAnchor.constraint(equalTo: rightArrow.leftAnchor, constant: -15).isActive = true
        name.bottomAnchor.constraint(equalTo: centerYAnchor, constant: 1).isActive = true
        
        addSubview(resultInfo)
        resultInfo.leftAnchor.constraint(equalTo: name.leftAnchor).isActive = true
        resultInfo.topAnchor.constraint(equalTo: centerYAnchor, constant: 2.5).isActive = true
    }
    
    func setupSearchResult(model: SearchRelevantResult?) {
        searchResultModel = model
        setupSearchResultView()
        setupInfo()
    }

    fileprivate func setupInfo() {
        guard let result = searchResultModel else {return}
        switch result.type {
        case .artist:
            Service.sharedInstance.spotifyService.getArtistProfile(id: result.id) { (artist) in
                self.profileImagePlaceholder.isHidden = !artist.profileImageUrl.isEmpty
                self.profileImage.layer.cornerRadius = self.frame.height/2
                self.profileImage.loadImage(urlString: artist.profileImageUrl)
                self.name.text = artist.name
                self.resultInfo.text = "ARTIST"
            }
        case .track:
            Service.sharedInstance.spotifyService.getTrack(id: result.id) { (track) in
                self.profileImagePlaceholder.isHidden = !track.album.imageUrl.isEmpty
                self.profileImage.layer.cornerRadius = 0
                self.profileImage.loadImage(urlString: track.album.imageUrl)
                self.name.text = track.name
                self.resultInfo.text = "Track • \(track.artists[0].name)"
            }
        default:
            break
        }
    }
    
}
