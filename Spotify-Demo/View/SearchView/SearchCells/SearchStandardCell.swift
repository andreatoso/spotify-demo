//
//  SearchCell.swift
//  Spotify-Demo
//
//  Created by Andrea Toso on 24/08/18.
//  Copyright © 2018 Andrea Toso. All rights reserved.
//

import UIKit

class SearchStandardCell: SearchBaseCell {
    
    var searchArtistModel: Artist?
    var searchPlaylistModel: Playlist?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupSearchArtistResult(model: Artist) {
        searchArtistModel = model
        setupArtistInfo()
    }
    
    func setupSearchPlaylistResult(model: Playlist) {
        searchPlaylistModel = model
        setupPlaylistInfo()
    }
    
    let name: UILabel = {
        let label = UILabel()
        label.text = "Shade"
        label.font = UIFont.init(name: Font.Medium, size: 16.5)
        label.textColor = UIColor.white
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    func setupViews() {
        addSubview(name)
        name.leftAnchor.constraint(equalTo: profileImage.rightAnchor, constant: 15).isActive = true
        name.rightAnchor.constraint(equalTo: rightArrow.leftAnchor, constant: -15).isActive = true
        name.anchorCenterYToSuperview()
    }
    
    fileprivate func setupArtistInfo() {
        guard let artist = searchArtistModel else {return}
        
        profileImagePlaceholder.isHidden = !artist.profileImageUrl.isEmpty
        profileImage.loadImage(urlString: artist.profileImageUrl)
        name.text = artist.name
    }
    
    fileprivate func setupPlaylistInfo() {
        guard let playlist = searchPlaylistModel else {return}
        
        profileImagePlaceholder.image = UIImage(named: "track_placeholder")
        profileImagePlaceholder.isHidden = !playlist.imageUrl.isEmpty
        profileImage.loadImage(urlString: playlist.imageUrl)
        name.text = playlist.name
    }
    
}
