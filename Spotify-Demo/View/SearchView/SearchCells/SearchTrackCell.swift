//
//  ArtistSearchCell.swift
//  Spotify-Demo
//
//  Created by Andrea Toso on 25/08/18.
//  Copyright © 2018 Andrea Toso. All rights reserved.
//

import UIKit

class SearchTrackCell: SearchBaseCell {
    
    var searchTrackModel: Track?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupTracksResult(model: Track) {
        searchTrackModel = model
        setupTrackResultViews()
        setupInfo()
    }

    let name: UILabel = {
        let label = UILabel()
        label.text = "Shade"
        label.font = UIFont.init(name: Font.Medium, size: 16.5)
        label.textColor = UIColor.white
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let artist: UILabel = {
        let label = UILabel()
        label.text = "Artista"
        label.font = UIFont.init(name: Font.Book, size: 14)
        label.textColor = Color.LightGray
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let explicitContainer: UIView = {
        let view = UIView()
        view.backgroundColor = Color.LightGray
        view.layer.cornerRadius = 3
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let explicit: UILabel = {
        let label = UILabel()
        label.text = "EXPLICIT"
        label.font = UIFont.init(name: Font.Book, size: 9)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    fileprivate func setupTrackResultViews() {
        layoutIfNeeded()
        
        guard let isExplicitBool = searchTrackModel?.isExplicit else {return}
        guard let artists = searchTrackModel?.artists else {return}
        
        profileImage.layer.cornerRadius = 0
        profileImage.isHidden = true
        
        addSubview(name)
        name.leftAnchor.constraint(equalTo: leftAnchor, constant: 15).isActive = true
        name.rightAnchor.constraint(equalTo: rightArrow.leftAnchor, constant: -15).isActive = true
        
        addSubview(explicitContainer)
        explicitContainer.anchor(centerYAnchor, left: leftAnchor, bottom: nil, right: nil, topConstant: 5, leftConstant: 15, bottomConstant: 0, rightConstant: 0, widthConstant: 46, heightConstant: 12.5)
        
        explicitContainer.addSubview(explicit)
        explicit.anchorCenterSuperview()
        
        addSubview(artist)
        artist.topAnchor.constraint(equalTo: centerYAnchor, constant: 2.5).isActive = true
        artist.rightAnchor.constraint(equalTo: rightArrow.leftAnchor, constant: -8).isActive = true
        
        explicitContainer.isHidden = !isExplicitBool
        artist.isHidden = artists.isEmpty
        
        if !isExplicitBool && artists.isEmpty {
            name.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        } else {
            name.bottomAnchor.constraint(equalTo: centerYAnchor, constant: 1).isActive = true
            
            if !artists.isEmpty {
                if !isExplicitBool {
                    artist.leftAnchor.constraint(equalTo: leftAnchor, constant: 15).isActive = true
                } else {
                    artist.leftAnchor.constraint(equalTo: explicitContainer.rightAnchor, constant: 8).isActive = true
                }
            }
        }
    }
    
    fileprivate func setupInfo() {
        guard let track = searchTrackModel else {return}
        let trackName = track.name
        let artists = track.artists
        let albumName = track.album.name
    
        name.text = trackName
        artist.text = ""
        artists.forEach { (text) in
            guard let artistText = artist.text else {return}
            artist.text = artistText.isEmpty ? "\(text.name)" : "\(artistText), \(text.name)"
        }
        guard let artistTextString = artist.text else {return}
        artist.text = "\(artistTextString) • \(albumName)"
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        // Resolve bug on search reload
        artist.removeFromSuperview()
    }
    
}
