//
//  HorizontalCVHeader.swift
//  Spotify-Demo
//
//  Created by Andrea Toso on 20/08/18.
//  Copyright © 2018 Andrea Toso. All rights reserved.
//

import UIKit

class SectionHeader: UICollectionViewCell {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let title: UILabel = {
        let label = UILabel()
        label.text = "Ascoltati di recente"
        label.textColor = UIColor.white
        label.font = UIFont(name: Font.Bold, size: 18)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    fileprivate func setupViews() {
        addSubview(title)
        title.anchorCenterXToSuperview()
        if frame.height == 45 {
            title.anchorCenterYToSuperview()
        } else {
            title.centerYAnchor.constraint(equalTo: centerYAnchor, constant: (frame.height-45)/2).isActive = true
        }
    }
    
}
