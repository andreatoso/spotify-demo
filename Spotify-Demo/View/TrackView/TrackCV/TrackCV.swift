//
//  TrackHeader.swift
//  Spotify-Demo
//
//  Created by Andrea Toso on 16/09/18.
//  Copyright © 2018 Andrea Toso. All rights reserved.
//

import UIKit

class TrackCV: UIView, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    let cellId = "horizontalCVCell"
    weak var delegate: TrackControllerDelegate?
    weak var libraryControllerDelagte: LibraryControllerDelegate?
    
    var tracks: [Track] = [] {didSet {self.collectionView.reloadData()}}
    
    var trackController: TrackController?
    
    var indexPath: IndexPath = IndexPath() {
        didSet{
            collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: false)
            let track = tracks[indexPath.item]
            if !oldValue.isEmpty {
                if currentTrack.id != track.id {
                    delegate?.playTrack(track: tracks[indexPath.item])
                    currentTrackIndex = indexPath
                } else {
                    currentTrackIndex = oldValue
                }
            } else {
                delegate?.playTrack(track: tracks[indexPath.item])
                currentTrackIndex = indexPath
            }
            currentTrack = track
        }
    }
    
    var currentTrack: Track!
    var currentTrackIndex = IndexPath()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = UIColor.clear
        cv.showsHorizontalScrollIndicator = false
        cv.isPagingEnabled = true
        cv.delegate = self
        cv.dataSource = self
        cv.translatesAutoresizingMaskIntoConstraints = false
        return cv
    }()
    
    fileprivate func setupViews() {
        collectionView.register(TrackCVCell.self, forCellWithReuseIdentifier: cellId)
        
        addSubview(collectionView)
        collectionView.fillSuperview()
    }
    
}

extension TrackCV {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tracks.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! TrackCVCell
        cell.trackImage.loadImage(urlString: tracks[indexPath.item].album.imageUrl)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.frame.size.width, height: self.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageWidth = scrollView.frame.size.width
        let page = Int(floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1)
        if page != currentTrackIndex.item {
            delegate?.playTrack(track: tracks[page])
            libraryControllerDelagte?.updateCurrentTrack(trackNumber: page)
            currentTrackIndex.item = page
            trackController?.indexPath = IndexPath(item: page, section: 0)
        }
    }
    
}
