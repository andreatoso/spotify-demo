//
//  TrackCVCell.swift
//  Spotify-Demo
//
//  Created by Andrea Toso on 17/09/18.
//  Copyright © 2018 Andrea Toso. All rights reserved.
//

import UIKit

class TrackCVCell: UICollectionViewCell {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let trackImage: CachedImageView = {
        let iv = CachedImageView()
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    fileprivate func setupViews() {
        let sizes = [frame.width, frame.height]
        guard let size = sizes.min() else {return}
        
        addSubview(trackImage)
        trackImage.anchorCenterSuperview()
        trackImage.widthAnchor.constraint(equalToConstant: size).isActive = true
        trackImage.heightAnchor.constraint(equalToConstant: size).isActive = true
    }
    
}
