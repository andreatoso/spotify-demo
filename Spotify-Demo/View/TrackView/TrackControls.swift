//
//  TrackHeader.swift
//  Spotify-Demo
//
//  Created by Andrea Toso on 16/09/18.
//  Copyright © 2018 Andrea Toso. All rights reserved.
//

import UIKit

class TrackControls: UIView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let likeButton: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "like")?.withRenderingMode(.alwaysTemplate), for: .normal)
        button.imageView?.tintColor = UIColor.white
        button.imageView?.contentMode = .scaleAspectFit
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    let previousButtonContainer: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let previousButton: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "previous_button")?.withRenderingMode(.alwaysTemplate), for: .normal)
        button.imageView?.tintColor = UIColor.white
        button.imageView?.contentMode = .scaleAspectFit
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    let playButton: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "play_button")?.withRenderingMode(.alwaysTemplate), for: .normal)
        button.imageView?.tintColor = UIColor.white
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    let nextButtonContainer: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let nextButton: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "next_button")?.withRenderingMode(.alwaysTemplate), for: .normal)
        button.imageView?.tintColor = UIColor.white
        button.imageView?.contentMode = .scaleAspectFit
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    let prohibitionButton: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "prohibition_button")?.withRenderingMode(.alwaysTemplate), for: .normal)
        button.imageView?.tintColor = UIColor.white
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    fileprivate func setupViews() {
        addSubview(likeButton)
        likeButton.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        likeButton.leftAnchor.constraint(equalTo: leftAnchor, constant: 25).isActive = true
        likeButton.heightAnchor.constraint(equalToConstant: 20).isActive = true
        likeButton.widthAnchor.constraint(equalToConstant: 20).isActive = true
        
        addSubview(playButton)
        playButton.anchorCenterSuperview()
        playButton.heightAnchor.constraint(equalToConstant: 65).isActive = true
        playButton.widthAnchor.constraint(equalToConstant: 65).isActive = true
        
        addSubview(prohibitionButton)
        prohibitionButton.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        prohibitionButton.rightAnchor.constraint(equalTo: rightAnchor, constant: -25).isActive = true
        prohibitionButton.heightAnchor.constraint(equalToConstant: 20).isActive = true
        prohibitionButton.widthAnchor.constraint(equalToConstant: 20).isActive = true
        
        addSubview(previousButtonContainer)
        previousButtonContainer.anchor(topAnchor, left: likeButton.rightAnchor, bottom: bottomAnchor, right: playButton.leftAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        previousButtonContainer.addSubview(previousButton)
        previousButton.anchorCenterSuperview()
        previousButton.heightAnchor.constraint(equalToConstant: 18).isActive = true
        previousButton.widthAnchor.constraint(equalToConstant: 18).isActive = true
        
        addSubview(nextButtonContainer)
        nextButtonContainer.anchor(topAnchor, left: playButton.rightAnchor, bottom: bottomAnchor, right: prohibitionButton.leftAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        nextButtonContainer.addSubview(nextButton)
        nextButton.anchorCenterSuperview()
        nextButton.heightAnchor.constraint(equalToConstant: 18).isActive = true
        nextButton.widthAnchor.constraint(equalToConstant: 18).isActive = true
    }
    
}
