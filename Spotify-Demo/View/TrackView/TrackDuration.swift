//
//  TrackHeader.swift
//  Spotify-Demo
//
//  Created by Andrea Toso on 16/09/18.
//  Copyright © 2018 Andrea Toso. All rights reserved.
//

import UIKit

class TrackDuration: UIView {
    
    var customTabBarController: CustomTabBarController?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let startDuration: UILabel = {
        let label = UILabel()
        label.text = "00:00"
        label.textColor = UIColor.white
        label.font = UIFont.init(name: Font.Book, size: 13)
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let endDuration: UILabel = {
        let label = UILabel()
        label.text = "00:00"
        label.textColor = UIColor.white
        label.font = UIFont.init(name: Font.Book, size: 13)
        label.textAlignment = .right
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let slider: UISlider = {
        let slider = UISlider()
        slider.minimumTrackTintColor = UIColor.white
        slider.maximumTrackTintColor = UIColor.white.withAlphaComponent(0.3)
//        slider.addTarget(self, action: #selector(handleSlider(slider:event:)), for: .allEvents)
        slider.translatesAutoresizingMaskIntoConstraints = false
        return slider
    }()
    
    @objc func handleSlider(slider: UISlider, event: UIEvent) {
        if let touchEvent = event.allTouches?.first {
            switch touchEvent.phase {
            case .began:
                customTabBarController?.config.panRecognier.isEnabled = false
            case .moved:
                slider.setThumbImage(getImage(image: UIImage(named: "slider_thumb_active"), scaledToSize: CGSize(width: 24, height: 24)), for: .normal)
                customTabBarController?.config.panRecognier.isEnabled = false
            case .ended:
                slider.setThumbImage(getImage(image: UIImage(named: "slider_thumb"), scaledToSize: CGSize(width: 24, height: 24)), for: .normal)
                customTabBarController?.config.panRecognier.isEnabled = true
            default:
                break
            }
        }
    }
    
    fileprivate func setupViews() {
        addSubview(startDuration)
        startDuration.leftAnchor.constraint(equalTo: leftAnchor, constant: 15).isActive = true
        startDuration.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        startDuration.widthAnchor.constraint(equalToConstant: 36).isActive = true
        
        addSubview(endDuration)
        endDuration.rightAnchor.constraint(equalTo: rightAnchor, constant: -15).isActive = true
        endDuration.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        endDuration.widthAnchor.constraint(equalToConstant: 36).isActive = true
        
        addSubview(slider)
        slider.anchorCenterYToSuperview()
        slider.leftAnchor.constraint(equalTo: startDuration.rightAnchor, constant: 15).isActive = true
        slider.rightAnchor.constraint(equalTo: endDuration.leftAnchor, constant: -15).isActive = true

        slider.setThumbImage(getImage(image: UIImage(named: "slider_thumb"), scaledToSize: CGSize(width: 24, height: 24)), for: .normal)
//        slider.setThumbImage(getImage(image: UIImage(named: "slider_thumb_active"), scaledToSize: CGSize(width: 24, height: 24)), for: .highlighted)
    }
    
    fileprivate func getImage(image: UIImage?, scaledToSize newSize: CGSize) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        guard let image = image else {return UIImage()}
        image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        guard let newImage = UIGraphicsGetImageFromCurrentImageContext() else {return UIImage()}
        UIGraphicsEndImageContext()
        return newImage
    }
    
}
