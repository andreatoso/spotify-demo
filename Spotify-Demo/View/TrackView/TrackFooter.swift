//
//  TrackHeader.swift
//  Spotify-Demo
//
//  Created by Andrea Toso on 16/09/18.
//  Copyright © 2018 Andrea Toso. All rights reserved.
//

import UIKit

class TrackFooter: UIView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let availableDevices: UILabel = {
        let label = UILabel()
        label.text = "Dispositivi disponibili"
        label.textColor = UIColor.white
        label.font = UIFont.init(name: Font.Book, size: 10.5)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let devicesButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setImage(UIImage(named: "devices"), for: .normal)
        button.imageView?.contentMode = .scaleAspectFit
        button.isUserInteractionEnabled = false
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    fileprivate func setupViews() {
        addSubview(availableDevices)
        availableDevices.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        availableDevices.centerXAnchor.constraint(equalTo: centerXAnchor, constant: 7).isActive = true
        
        addSubview(devicesButton)
        devicesButton.centerYAnchor.constraint(equalTo: availableDevices.centerYAnchor, constant: 1).isActive = true
        devicesButton.rightAnchor.constraint(equalTo: availableDevices.leftAnchor, constant: -5).isActive = true
        devicesButton.heightAnchor.constraint(equalToConstant: 11).isActive = true
        devicesButton.widthAnchor.constraint(equalToConstant: 12).isActive = true
    }
    
}
