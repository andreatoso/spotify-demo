//
//  TrackHeader.swift
//  Spotify-Demo
//
//  Created by Andrea Toso on 16/09/18.
//  Copyright © 2018 Andrea Toso. All rights reserved.
//

import UIKit

class TrackHeader: UIView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let reproduction: UILabel = {
        let label = UILabel()
        let attribute1 = [NSAttributedString.Key.kern: 1.1]
        let baseString = NSAttributedString(string: "RIPRODUZIONE DA", attributes: attribute1)
        label.attributedText = baseString
        label.textColor = Color.LightGray
        label.font = UIFont.init(name: Font.Book, size: 10.5)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let dots: UIButton = {
        let iv = UIButton()
        iv.setImage(UIImage(named: "dots")?.withRenderingMode(.alwaysTemplate), for: .normal)
        iv.imageView?.tintColor = Color.LightGray
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    let source: UILabel = {
        let label = UILabel()
        label.text = "Stoney (Deluxe)"
        label.textColor = Color.LightGray
        label.font = UIFont.init(name: Font.Book, size: 13.5)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let closePlayerButton: UIButton = {
        let iv = UIButton(type: .custom)
        iv.setImage(UIImage(named: "right_arrow")?.withRenderingMode(.alwaysTemplate), for: .normal)
        iv.imageView?.tintColor = Color.LightGray
        iv.imageView?.transform = CGAffineTransform(rotationAngle: .pi/2)
        iv.imageView?.contentMode = .scaleAspectFit
        iv.accessibilityLabel = "close"
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    fileprivate func setupViews() {
        addSubview(reproduction)
        reproduction.anchorCenterXToSuperview()
        reproduction.topAnchor.constraint(equalTo: topAnchor).isActive = true

        addSubview(dots)
        dots.topAnchor.constraint(equalTo: topAnchor).isActive = true
        dots.rightAnchor.constraint(equalTo: rightAnchor, constant: -22).isActive = true

        addSubview(closePlayerButton)
        closePlayerButton.topAnchor.constraint(equalTo: reproduction.topAnchor, constant: 0).isActive = true
        closePlayerButton.leftAnchor.constraint(equalTo: leftAnchor, constant: 22).isActive = true
        closePlayerButton.heightAnchor.constraint(equalToConstant: 19.5).isActive = true
        closePlayerButton.widthAnchor.constraint(equalToConstant: 19.5).isActive = true
        
        addSubview(source)
        source.anchorCenterXToSuperview()
        source.topAnchor.constraint(equalTo: reproduction.bottomAnchor, constant: 2).isActive = true
    }
    
}
