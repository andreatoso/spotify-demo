//
//  TrackHeader.swift
//  Spotify-Demo
//
//  Created by Andrea Toso on 16/09/18.
//  Copyright © 2018 Andrea Toso. All rights reserved.
//

import UIKit

class TrackName: UIView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let trackName: UILabel = {
        let label = UILabel()
        label.text = "I Fall Apart"
        label.textColor = UIColor.white
        label.font = UIFont.init(name: Font.Bold, size: 19)
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let trackArtist: UILabel = {
        let label = UILabel()
        label.text = "Post Malone"
        label.textColor = Color.LightGray
        label.font = UIFont.init(name: Font.Bold, size: 16)
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    fileprivate func setupViews() {
        addSubview(trackName)
        trackName.anchorCenterXToSuperview()
        trackName.topAnchor.constraint(equalTo: topAnchor).isActive = true
        trackName.widthAnchor.constraint(equalTo: widthAnchor, constant: -30).isActive = true
        
        addSubview(trackArtist)
        trackArtist.anchorCenterXToSuperview()
        trackArtist.topAnchor.constraint(equalTo: trackName.bottomAnchor, constant: 5).isActive = true
        trackArtist.widthAnchor.constraint(equalTo: widthAnchor, constant: -30).isActive = true
    }
    
}
