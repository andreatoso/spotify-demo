//
//  VerticalCell.swift
//  Spotify-Demo
//
//  Created by Andrea Toso on 22/08/18.
//  Copyright © 2018 Andrea Toso. All rights reserved.
//

import UIKit

class VerticalCell: UICollectionViewCell, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    let cellId = "cellId"
    var requestHelperCv: Bool? = false {
        didSet {
            if let request = requestHelperCv {
                if request { setupCV() }
            }
        }
    }
    
    var verticalModel: [Playlist] = []
    var featuredPlaylist: FeaturedPlaylist? { didSet {setupFeaturedPlaylist()} }
    
    weak var delegate: PlayerDelegate?
    weak var homeControllerDelegate: HomeControllerDelegate?
    var trackController: TrackController?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = UIColor.clear
        cv.delegate = self
        cv.dataSource = self
        cv.translatesAutoresizingMaskIntoConstraints = false
        return cv
    }()
    
    let playlistCover: CachedImageView = {
        let view = CachedImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let playlistCoverOverlay: UIImageView = {
        let iv = UIImageView()
        iv.backgroundColor = UIColor.cyan.withAlphaComponent(0.07)
        iv.isHidden = true
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    let playlistDescription: UILabel = {
        let label = UILabel()
        label.text = "Il tuo mixtape settimanale di musica sempre nuova e di chicche scelte solo pereeeeee"
        label.font = UIFont(name: Font.Book, size: 16)
        label.textColor = Color.LightGray
        label.numberOfLines = 2
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    fileprivate func setupViews() {
        addSubview(playlistCover)
        let squareRatio = self.frame.size.width/3
        playlistCover.topAnchor.constraint(equalTo: topAnchor).isActive = true
        playlistCover.widthAnchor.constraint(equalTo: widthAnchor, constant: -squareRatio).isActive = true
        playlistCover.heightAnchor.constraint(equalTo: playlistCover.widthAnchor).isActive = true
        playlistCover.anchorCenterXToSuperview()
        
        playlistCover.addSubview(playlistCoverOverlay)
        playlistCoverOverlay.fillSuperview()
        
        addSubview(playlistDescription)
        playlistDescription.anchorCenterXToSuperview()
        playlistDescription.anchor(playlistCover.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, topConstant: 8, leftConstant: 31, bottomConstant: 0, rightConstant: 31, widthConstant: 0, heightConstant: 0)
    }
    
    fileprivate func setupCV() {
        collectionView.register(HorizontalCVCell.self, forCellWithReuseIdentifier: cellId)
        addSubview(collectionView)
        collectionView.anchor(playlistDescription.bottomAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, topConstant: 55, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
    }
    
}

extension VerticalCell {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return verticalModel.count-1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! HorizontalCVCell
        cell.playlist = verticalModel[indexPath.item+1]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let libraryController = LibraryController(collectionViewLayout: UICollectionViewFlowLayout())
        libraryController.delegate = self.delegate
        libraryController.trackController = self.trackController
        
        libraryController.library = Library(id: verticalModel[indexPath.item+1].id, type: .playlist)
        self.homeControllerDelegate?.pushController(controller: libraryController)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 150, height: collectionView.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: 0, left: 20, bottom: 0, right: -20)
    }
    
}

extension VerticalCell {
    
    // MARK: Made for you playlists
    func setupMadeForYouPlaylists(model: [Playlist]) {
        self.verticalModel = model
        
        if !model.isEmpty {
            playlistCover.loadImage(urlString: model[0].imageUrl) {
                self.playlistCover.image = self.playlistCover.image
            }
            
            var description = ""
            model[0].tracks.forEach { (track) in
                track.artists.forEach({ (artist) in
                    description += "\(artist.name), "
                })
            }
            playlistDescription.text = description
        }
        
        self.collectionView.reloadData()
    }
    
    // MARK: Featured playlists
    internal func setupFeaturedPlaylist() {
        guard let urlString = featuredPlaylist?.imageUrl else {return}
        guard let description = featuredPlaylist?.name else {return}
        playlistCover.loadImage(urlString: urlString)
        playlistDescription.text = description
    }
    
}
